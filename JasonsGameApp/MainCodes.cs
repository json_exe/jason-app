﻿using JasonsGameApp;
using JasonsGameApp.Windows;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Windows.Forms;

namespace MainCodes
{
    public class Codes
    {   
        //Speichern des Pfades, wo sich das Programm befindet in einem String
        string CurrentDir = Directory.GetCurrentDirectory();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void LoadMainWindow(NewMenu menu)
        {
            
        }

        #region DownloadAssistant

        string NameOfDownload;
        DownloadWizard downloaded;
        WebClient downloadClient;
        public void WhatToDownload(DownloadWizard download, string NameOfDownload)
        {
            this.NameOfDownload = NameOfDownload;
            string PathCurrent = Directory.GetCurrentDirectory();
            this.downloaded = download;
            if (Directory.Exists(PathCurrent + "\\Downloads"))
            {
                Directory.CreateDirectory(PathCurrent + "\\Downloads");
                log.Info("Creating Download Directory");
            }
            if (NameOfDownload == "Download1")
            {
                log.Info("Downloading DokiDokiGer");
                downloadClient = new WebClient();
                downloadClient.DownloadFileCompleted += client_DownloadFileCompleted2;
                downloadClient.DownloadProgressChanged += client_DownloadProgressChanged2;
                downloadClient.DownloadFileAsync(new Uri("https://www.dropbox.com/s/7cia9astgvxikul/DokiDokiGerman.zip?dl=1"), PathCurrent + "\\DokiDokiGerman.zip");
                MessageBox.Show("Der Download wird nun gestartet.", "Download Start", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (NameOfDownload == "Download2")
            {
                log.Info("Downloading DJMixFull");
                downloadClient = new WebClient();
                downloadClient.DownloadFileCompleted += client_DownloadFileCompleted2;
                downloadClient.DownloadProgressChanged += client_DownloadProgressChanged2;
                downloadClient.DownloadFileAsync(new Uri("https://onedrive.live.com/download?cid=156EED1AED4F8A36&resid=156EED1AED4F8A36%2131945&authkey=AA_8mcUGMeXgD70"), PathCurrent + "\\Downloads\\DJJasonandPinkyMix.mp3");
                MessageBox.Show("Der Download wird nun gestartet.", "Download Start", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (NameOfDownload == "Download3")
            {
                log.Info("Downloading Newestmix");
                downloadClient = new WebClient();
                downloadClient.DownloadFileCompleted += client_DownloadFileCompleted2;
                downloadClient.DownloadProgressChanged += client_DownloadProgressChanged2;
                downloadClient.DownloadFileAsync(new Uri("https://onedrive.live.com/download?cid=156EED1AED4F8A36&resid=156EED1AED4F8A36%2131271&authkey=AHmCY3DIXNpWSYo"), PathCurrent + "\\Downloads\\NewestMix.mp3");
                MessageBox.Show("Der Download wird nun gestartet.", "Download Start", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (NameOfDownload == "Download4")
            {
                log.Info("Downloading Spammer");
                downloadClient = new WebClient();
                downloadClient.DownloadFileCompleted += client_DownloadFileCompleted2;
                downloadClient.DownloadProgressChanged += client_DownloadProgressChanged2;
                downloadClient.DownloadFileAsync(new Uri("https://jsprivatenextcloud.ddns.net/index.php/s/n3eesAtAYzN6FTZ/download/spammer.exe"), PathCurrent + "\\Downloads\\EZSpammer.exe");
                MessageBox.Show("Der Download wird nun gestartet.", "Download Start", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void client_DownloadFileCompleted2(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (Directory.Exists(CurrentDir + "\\Downloads"))
            {

            }
            else
            {
                Directory.CreateDirectory(CurrentDir + "\\Downloads");
            }
            if (NameOfDownload == "Download1")
            {
                if (Directory.Exists(CurrentDir + "\\DokiDokiGerman"))
                {
                    log.Info("Starting DokiDoki");
                    MessageBox.Show("Datei Erfolgreich Heruntergeladen! Bitte drücke OK um zum Letzten Schritt zu kommen");
                    Directory.Delete(CurrentDir + "\\DokiDokiGerman", true);
                    ZipFile.ExtractToDirectory(CurrentDir + "\\DokiDokiGerman.zip", CurrentDir);
                    File.Delete(CurrentDir + "\\DokiDokiGerman.zip");
                    MessageBox.Show("Installation Erfolgreich! Du kannst nun das Installations Fenster schließen. Die Installierte Anwendung wird " +
                        "dabei einmal gestartet!");
                    Process.Start(CurrentDir + "\\DokiDokiGerman\\DDLC.exe");
                } 
            }
            else if (NameOfDownload == "Download2")
            {
                log.Info("Starting DJMixFull");
                //Wenn der Download Abgeschlossen ist, wird das hier ausgeführt
                MessageBox.Show("Download Abgeschlossen! Drücken sie OK um die Datei auszuführen");
                //Extrahiert die Zip Datei, löscht nach dem Extrahieren und führt die exe dann aus.;
                Process.Start(CurrentDir + "\\Downloads\\DJJasonandPinkyMix.mp3");
                MessageBox.Show("Download Abgeschlossen! Sie können dieses Fenster nun schließen.");
            }
            else if (NameOfDownload == "Download3")
            {
                log.Info("Starting NewestMix");
                //Wenn der Download Abgeschlossen ist, wird das hier ausgeführt
                MessageBox.Show("Download Abgeschlossen! Drücken sie OK um die Datei auszuführen");
                Process.Start(CurrentDir + "\\Downloads\\NewestMix.mp3");
                MessageBox.Show("Download Abgeschlossen! Sie können dieses Fenster nun schließen.");
            }
            else if (NameOfDownload == "Download4")
            {
                log.Info("Starting Spammer");
                //Wenn der Download Abgeschlossen ist, wird das hier ausgeführt
                MessageBox.Show("Download Abgeschlossen! Drücken sie OK um die Datei auszuführen");
                Process.Start(CurrentDir + "\\Downloads\\EZSpammer.exe");
                MessageBox.Show("Download Abgeschlossen! Sie können dieses Fenster nun schließen.");
            }
        }

        private void client_DownloadProgressChanged2(object sender, DownloadProgressChangedEventArgs e)
        {
            //Je nachdem wie der Download voranschreitet, wird dies in der Progress Bar angezeigt.
            downloaded.pbDownloadProgress.Value = e.ProgressPercentage;
            double RecievedMegabytes = ConvertBytesToMegabytes(e.BytesReceived);
            double TotalMegabytes = ConvertBytesToMegabytes(e.TotalBytesToReceive);
            downloaded.lblDownloadInfo.Content = RecievedMegabytes.ToString() + " from " + TotalMegabytes.ToString();
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public void FormClosing2()
        {
            string fileName = CurrentDir + "\\update.txt";
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            //Wenn der WebClient noch läuft, soll dieser nun Deaktiviert und Zerstört werden
            if (downloadClient != null)
            {
                try
                {
                    downloadClient.CancelAsync();
                    downloadClient.Dispose();
                }
                catch (Exception exe)
                {
                    MessageBox.Show(exe.ToString());
                }
            }
            else
            {

            }
        }
        #endregion
    }
}