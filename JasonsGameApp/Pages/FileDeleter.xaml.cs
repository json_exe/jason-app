﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Microsoft.Win32;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für FileDeleter.xaml
    /// </summary>
    public partial class FileDeleter : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FileDeleter()
        {
            InitializeComponent();
            log.Info("Instanciated FileDeleter page");
        }

        private void btnAddFiles_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Title = "Dateien zum Löschen auswählen";
            openFileDialog.InitialDirectory = "C:\\";
            openFileDialog.Multiselect = true;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            log.Info("Opening File-Dialog");
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in openFileDialog.FileNames)
                {
                    log.Debug("Adding Files to list File: " + file);
                    listAddedFiles.Items.Add(file);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (listAddedFiles.Items != null)
            {
                DialogResult result = (DialogResult)System.Windows.MessageBox.Show("Die Daten sind nach der Löschung nicht wiederherstellbar! Möchten sie Fortfahren?", "Fortfahren", MessageBoxButton.YesNo);
                if (result == DialogResult.Yes)
                {
                    log.Info("User accepted Warning");
                    foreach (string path in listAddedFiles.Items)
                    {
                        log.Debug("Deleting File. File: " + path);
                        try
                        {
                            File.Delete(path);
                        }
                        catch (System.Exception ex)
                        {
                            log.Error("Failed to Delete File! ", ex);
                            System.Windows.Forms.MessageBox.Show("Datei konnte nicht gelöscht werden! Pfad: " + path);
                        }
                    }
                    listAddedFiles.Items.Clear();
                    log.Debug("Files Cleared");
                }
                else
                {
                    listAddedFiles.Items.Clear();
                    System.Windows.MessageBox.Show("Löschvorgang abgebrochen. Liste wurde geleert!");
                }
                
            }
            else
            {
                System.Windows.MessageBox.Show("Bitte füge Dateien der Liste hinzu!");
            }
        }

        private void btnDeleteSelectedFile_Click(object sender, RoutedEventArgs e)
        {
            if (listAddedFiles.SelectedItem != null)
            {
                listAddedFiles.Items.Remove(listAddedFiles.SelectedItem);
            }
            else
            {
                System.Windows.MessageBox.Show("Bitte wähle einen Eintrag aus der Liste!");
            }
        }
    }
}
