﻿using JasonsGameApp.CodeDateien;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für UserWallet.xaml
    /// </summary>
    public partial class UserWallet : Page
    {
        UserWalletCodes codes;
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        int WalletCount;

        public UserWallet()
        {
            InitializeComponent();
            codes = new UserWalletCodes();
            bool IsSecured = codes.CheckWalletSetting();
            if (IsSecured == false)
            {
                SecureWalletDisplay.Visibility = Visibility.Hidden;
                MainWalletDisplay.Visibility = Visibility.Visible;
                lblWalletFrom.Content = "Wallet von " + runTimeSave.UserName;
                WalletCount = codes.LoadWalletCount();
                txtBoxShowWalletCount.Text = $"Guthaben: {WalletCount} DevCoins";
            }
            else
            {
                if (runTimeSave.WalletUnlocked)
                {
                    SecureWalletDisplay.Visibility = Visibility.Hidden;
                    MainWalletDisplay.Visibility = Visibility.Visible;
                    lblWalletFrom.Content = "Wallet von " + runTimeSave.UserName;
                    WalletCount = codes.LoadWalletCount();
                    txtBoxShowWalletCount.Text = $"Guthaben: {WalletCount} DevCoins";
                }
                else
                {
                    MainWalletDisplay.Visibility = Visibility.Hidden;
                    SecureWalletDisplay.Visibility = Visibility.Visible;
                }
            }
        }

        private void btnUnlockWallet_Click(object sender, RoutedEventArgs e)
        {
            if (codes.UnlockWallet(pswdBoxEnterWalletPassword.Password, this))
            {
                SecureWalletDisplay.Visibility = Visibility.Hidden;
                MainWalletDisplay.Visibility = Visibility.Visible;
                lblWalletFrom.Content = "Wallet von " + runTimeSave.UserName;
                WalletCount = codes.LoadWalletCount();
                txtBoxShowWalletCount.Text = $"Guthaben: {WalletCount} DevCoins";
            }
        }

        private void btnShowTaskPage_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Tasks());
        }
    }
}
