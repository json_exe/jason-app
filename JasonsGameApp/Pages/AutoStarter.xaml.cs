﻿using JasonsGameApp.CodeDateien;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für AutoStarter.xaml
    /// </summary>
    public partial class AutoStarter : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        public AutoStarter()
        {
            InitializeComponent();
            log.Info("Instanciated AutoStarter Page");
        }

        //Adding a Website to the AutoStart list
        private void btnAddWebsite_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) return;
            if (txtBoxEnterWebsiteAdress.Text == string.Empty || txtBoxEnterWebsiteAdress.Text == "Website-Adresse")
            {
                MessageBox.Show("Bitte gebe zuerst etwas in die TextBox ein!", "Eingabe Ungültig");
            }
            else
            {
                //Creating a Regex for checking Website
                Regex regex = new Regex(@"(?<schema>https|http):\/\/(?<host>[\w\W]*?(\.net|\.com|.de|.eu))(?<path>\/.*)");
                bool IsWebsite = regex.IsMatch(txtBoxEnterWebsiteAdress.Text);
                if (IsWebsite == true)
                {
                    //Spollte die Webseite das Regex gepasst haben, wird geprüft ob sie überhaupt existiert
                    //Erst dann wird sie der Liste hinzugefügt
                    if (WebsiteExists(txtBoxEnterWebsiteAdress.Text) == true)
                    {
                        listShowAddedFiles.Items.Add(txtBoxEnterWebsiteAdress.Text);
                        log.Debug("Added Website Successfully");
                        MessageBox.Show("Added Website!");
                    }
                    else
                    {
                        MessageBox.Show("Diese Webseite existiert nicht oder ist momentan nicht erreichbar!");
                        log.Error("Cant Add Website, website doesnt Exist or is offline");
                    }

                }
                else
                {
                    MessageBox.Show("Bitte gebe eine gültige Adresse ein!", "No Valid Website");
                }
            }
        }

        //Öffnet einen Dateidialog indem man AUSFÜHRBARE Dateien hinzufügen kann
        private void btnAddFiles_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Start Instanciating File Dialog");
            int Error = 0;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Executables (*.EXE;*.BAT;*.LNK;*INK)|*.EXE;*.BAT;*.LNK;*INK";
            ofd.Title = "Ausführbare Dateien auswählen";
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Multiselect = true;
            ofd.InitialDirectory = "C:\\";
            log.Info("Opening File Dialog");
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //Fügt die Dateien der Liste hinzu wenn man im Dateidialog auf OK klickt
                log.Debug("Adding Files to List");
                foreach (string file in ofd.FileNames)
                {
                    try
                    {   
                        listShowAddedFiles.Items.Add(file);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to Open a File. File: " + file + "Inner Exception: ", ex);
                        Error++;
                    }
                }
                if (Error > 0)
                {
                    MessageBox.Show($"Beim hinzufügen der Datei/en sind Fehler aufgetreten. Insgesammt sind {Error} Fehler aufgetreten. Mögliche Probleme könnten sein: \n " +
                        $"-Die Datei wurde gelöscht \n -Der Pfad ist Fehlerhaft/Existiert nicht \n -Die Datei wurde verschoben \n -Die Datei erfordert erhöhte Zugriffsrechte \n " +
                        $"Sollte die Datei höhere Zugriffsrechte benötigen, so starten sie bitte das Programm als Administrator! \n Sollte das Problem weiterhin bestehen " +
                        $"melden sie sich bitte bei unserem Support mit der neusten Log Datei.");
                }
            }
        }

        //Startet die hinzugefügten Dateien und Webseiten aus der Liste
        private void btnStartAddedFiles_Click(object sender, RoutedEventArgs e)
        {
            //Versucht jede einzelne Datei zu starten. Bei Fehlern wird abgebrochen.
            foreach (string files in listShowAddedFiles.Items)
            {
                try
                {
                    Process.Start(files);
                    log.Debug($"Successfully Started Process: {files}");
                }
                catch (Exception ex)
                {
                    log.Error($"Failed Opening File {files}", ex);
                    MessageBox.Show($"Fehler beim öffnen der Datei {files}. Bitte überprüfen sie: \n -Das die Date existiert \n -Das die Datei nicht " +
                        "verschoben wurde \n -Starten sie die Anwendung einmal als Administrator \n" +
                        " Andernfalls kontatkieren sie bitte den Support und senden sie die neuste Log Datei mit.");
                }
            }
        }

        //Methode um ein Template seiner AutoStart objekte hinzuzufügen
        private void btnSaveTemplate_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Starting Template Saving");
            //Prüft zuerst ob dinge in der Liste sind
            if (listShowAddedFiles.Items == null)
            {
                log.Debug("File list is Empty");
                MessageBox.Show("Bitte füge Dateien der Liste hinzu!");
            }
            else
            {
                //Prüft ob ein name in das Text Feld eingegeben wurde
                if (txtBoxEnterNameToSave.Text == null || txtBoxEnterNameToSave.Text == "Name")
                {
                    log.Debug("No name Entered in Text Field");
                    MessageBox.Show("Bitte gebe einen Namen in das Textfeld ein!");
                }
                else
                {
                    Regex regex = new Regex(@"[A-Za-z0-9]");
                    bool regexTrue = regex.IsMatch(txtBoxEnterNameToSave.Text);
                    //Prüft ein 2 Mal das nur bestimmte Zeichen eingegeben wurden beim namen
                    if (regexTrue == true)
                    {
                        //Guckt ob ein template mit dem namen bereits existiert
                        if (File.Exists(Directory.GetCurrentDirectory() + "\\Templates\\" + txtBoxEnterNameToSave.Text + ".txt"))
                        {
                            log.Debug("Template Exists");
                            DialogResult result = MessageBox.Show("Template Existiert bereits. Möchte sie es Überschreiben?", "Datei Existiert", MessageBoxButtons.YesNo);
                            if (result == DialogResult.Yes)
                            {
                                //Überschreibt das alte Template indem er das alte Löscht und das neue erstellt
                                log.Info("Override old template");
                                File.Delete(Directory.GetCurrentDirectory() + "\\Templates\\" + txtBoxEnterNameToSave.Text + ".txt");
                                log.Debug("Deleted old template");
                                foreach (string item in listShowAddedFiles.Items)
                                {
                                    //Fügt der Textdatei die Einträge aus der Liste hinzu
                                    File.AppendAllText(Directory.GetCurrentDirectory() + "\\Templates\\" + txtBoxEnterNameToSave.Text + ".txt", item + Environment.NewLine);
                                    log.Debug("Wrote List Item into Template. Item: " + item);
                                }
                                MessageBox.Show("Template angelegt. Drücken sie OK um den AutostartHelper zu starten.");
                                try
                                {
                                    //Startet das AutoStartHelperTool
                                    Process.Start(Directory.GetCurrentDirectory() + "\\AutostartHelperTool.exe");
                                    log.Debug("Successfully Started AutostartHelperTool");
                                }
                                catch (Exception ex)
                                {
                                    log.Error("Failed to Open AutostartHelperTool!", ex);
                                    MessageBox.Show("Fehler beim öffnen des Hilfe Tools! Bitte wenden sie sich an den Support und senden sie die" +
                                        " neuste Log Datei mit!");
                                }
                            }
                            else
                            {
                                log.Debug("Stopped Override Process");
                                MessageBox.Show("Vorgang abgebrochen. Bitte ändern sie den Dateinamen!");
                            }
                        }
                        else
                        {
                            //Legt ein neues Template an
                            log.Debug("Template doesnt Exist");
                            //Guckt ob der Template Ordner existiert und legt einen an wenn dieser nicht existiert
                            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\Templates"))
                            {
                                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Templates");
                            }
                            //Schreibt den Pfad zur Datei in eine Textdatei
                            foreach (string item in listShowAddedFiles.Items)
                            {
                                File.AppendAllText(Directory.GetCurrentDirectory() + "\\Templates\\" + txtBoxEnterNameToSave.Text + ".txt", item + Environment.NewLine);
                                log.Debug("Wrote List Item into Template. Item: " + item);

                            }
                            MessageBox.Show("Template angelegt. Drücken sie OK um den AutostartHelper zu starten.");
                            try
                            {
                                //Startet das AutoHelperTool
                                Process.Start(Directory.GetCurrentDirectory() + "\\AutostartHelperTool.exe");
                                log.Debug("Successfully Started AutostartHelperTool");
                            }
                            catch (Exception ex)
                            {
                                log.Error("Failed to Open AutostartHelperTool!", ex);
                                MessageBox.Show("Fehler beim öffnen des Hilfe Tools! Bitte wenden sie sich an den Support und senden sie die" +
                                    " neuste Log Datei mit!");
                            }
                        }
                    }
                    else
                    {
                        log.Debug("Name does not match Criterias");
                        MessageBox.Show("Der Name verstößt gegen die Kriterien!" +
                            " Bitte geben sie nur einen Namen mit diesen " +
                            "Kriterien ein: \n A-Z \n a-z \n 0-9 \n Keine " +
                            "Leerzeichen oder sonstige Sonderzeichen!");
                    }
                }
            }
        }

        //Prüft ob die Webseite existiert
        private bool WebsiteExists(string Url)
        {
            log.Debug("Adding a Website");
            try
            {
                //Sendet eine HttpWebResponse mit der Methode HEAD an die URL
                HttpWebRequest request = WebRequest.Create(Url) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                response.Close();
                log.Debug($"Checked Website! Got Response Code: {response.StatusCode} on Website: {Url}");
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Error($"WebsiteCheckException. Cant Check Website {Url}.", ex);
                MessageBox.Show($"Failed to check Website {Url}! Bitte überprüfen sie: \n -Das die Adresse richtig geschrieben wurde \n -Das sie eine " +
                    "Inet Verbindung haben \n Andernfalls kontaktieren sie bitte den Support und senden sie die neuste Log Datei mit!");
                return false;
            }
        }

        private void btnDeleteSelectedFile_Click(object sender, RoutedEventArgs e)
        {
            if (listShowAddedFiles.SelectedItem == null)
            {
                MessageBox.Show("Bitte wähle zuerst einen Eintrag aus der Liste!");
            }
            else
            {
                listShowAddedFiles.Items.Remove(listShowAddedFiles.SelectedItem);
            }
        }
    }
}
