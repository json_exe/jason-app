﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.Windows;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für WelcomePage.xaml
    /// </summary>
    public partial class WelcomePage : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        public NewMenu menu = runTimeSave.menu;

        public WelcomePage()
        {
            InitializeComponent();
            NewsCode code = new NewsCode();
            //Ruft den Code auf um den text für die News Section zu erhalten
            code.InitNews(this);
            log.Info("Instanciated Welcome Page");
        }

        private void btnDiscordWebsite_Click(object sender, RoutedEventArgs e)
        {
            //Öffnet die Angegebene Webseite
            Process.Start("https://discord.com/invite/hSdy3QdDZK");
            log.Debug("Opening Website");
        }

        private void btnDiscordServerWebsite_Click(object sender, RoutedEventArgs e)
        {
            //Öffnet die Angegebene Webseite
            Process.Start("https://discord.com/invite/Fvnk6AvAC8");
            log.Debug("Opening Website");
        }

        private void btnDiscordInstagramWebsite_Click(object sender, RoutedEventArgs e)
        {
            //Öffnet die Angegebene Webseite
            Process.Start("https://www.instagram.com/talkingandgamingofficial/");
            log.Debug("Opening Website");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnCallWebsite_Click(object sender, RoutedEventArgs e)
        {
            //Öffnet die Angegebene Webseite
            Process.Start("https://jsprivatenextcloud.ddns.net/");
        }
    }
}
