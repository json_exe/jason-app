﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public UserControl1()
        {
            InitializeComponent();
            log.Info("Instanciated UserControl1");
        }

        private void btnStable_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            txtBoxSelection.Text = btn.Content.ToString();
        }

    }
}
