﻿using System.Windows;
using System.Windows.Controls;
using JasonsGameApp.CodeDateien;
using JasonsGameApp.localStore;
using ToastNotifications.Messages;
using JasonsGameApp.UserControls;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für Tasks.xaml
    /// </summary>
    public partial class Tasks : Page
    {

        private static readonly TaskCodes taskCodes = TaskCodes.Instance;
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        public Tasks()
        {
            InitializeComponent();
            TaskLogIn.Visibility = TaskSettings.Default.ShowLoginTask;
            if (taskCodes.AppStartedAtNewDay == true)
            {
                TaskStartApp.TaskCompleted.IsChecked = true;
            }
            if (taskCodes.UserLogsIn == true)
            {
                TaskLogIn.TaskCompleted.IsChecked = true;
            }
            for (int i = 0; i < 3; i++)
            {
                UserControls.UserControl1 taskControl = new UserControls.UserControl1();
                taskControl.TaskType = "TestTask";
                taskControl.Text = "Test Task";
                taskControl.ButtonText = "Dies ist eine Task zum testen";
                taskControl.Width = 782;
                taskControl.Margin = new Thickness(0, 5, 0, 0);
                stackPanel.Children.Add(taskControl);
            }
        }

        private void btnCollectTaskRevenue_Click(object sender, RoutedEventArgs e)
        {
            if (TaskStartApp.TaskCompleted.IsChecked == true)
            {
                taskCodes.CollectableDevCoins += 5;
                TaskStartApp.TaskCompleted.IsChecked = false;
                taskCodes.AppStartedAtNewDay = false;
            }
            if (TaskLogIn.TaskCompleted.IsChecked == true && TaskLogIn.Visibility != Visibility.Collapsed)
            {
                taskCodes.CollectableDevCoins += 20;
                TaskLogIn.Visibility = Visibility.Collapsed;
                TaskSettings.Default.ShowLoginTask = Visibility.Collapsed;
                TaskSettings.Default.Save();
            }
            if (taskCodes.CollectableDevCoins != 0)
            {
                taskCodes.AddDevCoins();
            }
            else
            {
                runTimeSave.notifier.ShowInformation("Es sind keine Tasks abgeschlossen!");
            }   
        }
    }
}
