﻿using JasonsGameApp.CodeDateien;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp
{
    /// <summary>
    /// Interaktionslogik für DownloadCenter.xaml
    /// </summary>
    public partial class DownloadCenter : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        public string WhichDownload;

        public DownloadCenter()
        {
            InitializeComponent();
            log.Info("Instanciated DownloadCenter page");
        }

        private void Download_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) return;
            Button btn = sender as Button;
            WhichDownload = btn.Name;
            log.Debug("Get Download Info");
            DialogResult result = MessageBox.Show("Möchten sie den Download starten?", "Starten?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                DownloadWizard download = new DownloadWizard(this);
                log.Info("Instanciated DownloadWizard");
                download.ShowDialog();
                log.Debug("Showing DownloadWizard");
            }
            else
            {
                MessageBox.Show("Abgebrochen!", "Aborted", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
