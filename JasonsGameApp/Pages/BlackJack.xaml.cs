﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp
{
    /// <summary>
    /// Interaktionslogik für BlackJack.xaml
    /// </summary>
    public partial class BlackJack : Page
    {
        bool Player1Passed, Player2Passed;
        int ScoreP1, ScoreP2;
        int Turn;
        int Player1CardValue, Player2CardValue, Value, MatchInfo = 1;

        private async void btnPass_Click(object sender, RoutedEventArgs e)
        {
            btnPass.IsEnabled = false;
            if (Turn == 1)
            {
                MessageBox.Show("Du setzt für das gesammte Spiel aus.");
                Player1Passed = true;
                if (Player2Passed != true)
                {
                    lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 2!";
                    lblCompleteCardValue.Content = "Gesammter Karten Wert: " + Player2CardValue;
                    lblCurrentCardValue.Content = "";
                    Turn++;
                }
            }
            else
            {
                MessageBox.Show("Du setzt für das gesammte Spiel aus.");
                Player2Passed = true;
                if (Player1Passed != true)
                {
                    lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 1!";
                    lblCompleteCardValue.Content = "Gesammter Karten Wert: " + Player1CardValue;
                    lblCurrentCardValue.Content = "";
                    Turn--;
                }
            }
            MatchInfo = Win();
            WhoWin();
            await Task.Delay(1000);
            btnPass.IsEnabled = true;
        }

        public BlackJack()
        {
            InitializeComponent();
            Random random = new Random();
            Turn = random.Next(1, 2);
            if (Turn == 1)
            {
                lblCurrentPlayer.Content = "Akutell am Zug: Spieler 1!";
            }
            else
            {
                lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 2!";
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private int Win()
        {
            if (Player1CardValue > 21 | Player2CardValue > 21)
            {
                if (Turn == 1)
                {
                    return 3;
                }
                else
                {
                    return 2;
                }
            }
            else if (Player1CardValue == 21 | Player2CardValue == 21)
            {
                if (Turn == 1)
                {
                    return 2;
                }
                else
                {
                    return 3;
                }
            }
            else if (Player1Passed == true & Player2Passed == true)
            {
                if (Player1CardValue > Player2CardValue)
                {
                    return 2;
                }
                else if (Player1CardValue < Player2CardValue)
                {
                    return 3;
                }
                else
                {
                    return 4;
                }
            }
            else
            {
                return 1;
            }
        }

        private async void btnTake_Click(object sender, RoutedEventArgs e)
        {
            btnTake.IsEnabled = false;
            if (Turn == 1)
            {
                if (Player1Passed == true)
                {
                    lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 2!";
                    lblCompleteCardValue.Content = "Gesammter Karten Wert: " + Player2CardValue;
                }
                else
                {
                    Random TakeCard = new Random();
                    Value = TakeCard.Next(1, 11);
                    Player1CardValue = Value + Player1CardValue;
                    lblCurrentCardValue.Content = "Wert der gezogenen Karte: " + Value;
                    lblCompleteCardValue.Content = "Gesammter Karten Wert:" + Player1CardValue;
                    await Task.Delay(3000);
                    if (Player2Passed != true)
                    {
                        lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 2!";
                        lblCompleteCardValue.Content = "Gesammter Karten Wert: " + Player2CardValue;
                        lblCurrentCardValue.Content = "";
                        Turn++;
                    }
                    else
                    {
                        lblCurrentPlayer.Content = "Spieler 2 setzt aus.";
                        await Task.Delay(1500);
                        lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 1!";
                    }
                }
            }
            else
            {
                if (Player2Passed == true)
                {
                    lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 1!";
                    lblCompleteCardValue.Content = "Gesammter Karten Wert:" + Player1CardValue;
                }
                else
                {
                    Random TakeCard = new Random();
                    Value = TakeCard.Next(1, 11);
                    Player2CardValue = Value + Player2CardValue;
                    lblCurrentCardValue.Content = "Wert der gezogenen Karte: " + Value;
                    lblCompleteCardValue.Content = "Gesammter Karten Wert: " + Player2CardValue;
                    await Task.Delay(3000);
                    if (Player1Passed != true)
                    {
                        lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 1!";
                        lblCompleteCardValue.Content = "Gesammter Karten Wert: " + Player1CardValue;
                        lblCurrentCardValue.Content = "";
                        Turn--;
                    }
                    else
                    {
                        lblCurrentPlayer.Content = "Spieler 1 setzt aus.";
                        await Task.Delay(1500);
                        lblCurrentPlayer.Content = "Aktuell am Zug: Spieler 2!";
                    }
                }
            }
            MatchInfo = Win();
            WhoWin();
            await Task.Delay(1000);
            btnTake.IsEnabled = true;
        }

        private void WhoWin()
        {
            if (MatchInfo != 1)
            {
                if (MatchInfo == 3)
                {
                    MessageBox.Show("Spieler 2, du hast Gewonnen!");
                    lblScoreboardPlayer2Score.Content = ++ScoreP2;
                }
                else if (MatchInfo == 2)
                {
                    MessageBox.Show("Spieler 1, du hast Gewonnen!");
                    lblScoreboardPlayer1Score.Content = ++ScoreP1;
                }
                else if (MatchInfo == 4)
                {
                    MessageBox.Show("Unentschieden! Keiner Gewinnt.");
                }
                MatchInfo = 1;
                Player1CardValue = 0;
                Value = 0;
                Player2CardValue = 0;
                Player1Passed = false;
                Player2Passed = false;
                lblCurrentCardValue.Content = "Wert der gezogenen Karte: ";
                lblCompleteCardValue.Content = "Gesammt Wert der Karten: ";
                if (Turn == 2)
                {
                    lblCurrentPlayer.Content = "Du bist am Zug, Spieler 1";
                }
                else
                {
                    lblCurrentPlayer.Content = "Du bist am Zug, Spieler 2";
                }
            }
            else
            {

            }
        }
    }
}
