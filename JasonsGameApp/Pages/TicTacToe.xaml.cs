﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp
{
    /// <summary>
    /// Interaktionslogik für TicTacToe.xaml
    /// </summary>
    public partial class TicTacToe : Page
    {
        int turn;
        int i, j;
        public TicTacToe()
        {
            this.InitializeComponent();
            Random rand = new Random();
            turn = rand.Next(1, 2);
            if (turn == 1)
            {
                lblCurrentPlayer.Content = "Aktuell am Zug: O";
            }
            else
            {
                lblCurrentPlayer.Content = "Aktuell am Zug: X";
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            foreach (Button button in wrapPanel.Children)
            {
                button.Content = "";
                button.IsEnabled = true;
            }
        }

        private void Win(string btnContent)
        {
            if ((btnTicTacToeGrid1.Content.ToString() == btnContent & btnTicTacToeGrid2.Content.ToString() == btnContent &
                 btnTicTacToeGrid3.Content.ToString() == btnContent)
               | (btnTicTacToeGrid1.Content.ToString() == btnContent & btnTicTacToeGrid4.Content.ToString() == btnContent &
                 btnTicTacToeGrid7.Content.ToString() == btnContent)
               | (btnTicTacToeGrid1.Content.ToString() == btnContent & btnTicTacToeGrid5.Content.ToString() == btnContent &
                 btnTicTacToeGrid9.Content.ToString() == btnContent)
               | (btnTicTacToeGrid2.Content.ToString() == btnContent & btnTicTacToeGrid5.Content.ToString() == btnContent &
                 btnTicTacToeGrid8.Content.ToString() == btnContent)
               | (btnTicTacToeGrid3.Content.ToString() == btnContent & btnTicTacToeGrid6.Content.ToString() == btnContent &
                 btnTicTacToeGrid9.Content.ToString() == btnContent)
               | (btnTicTacToeGrid4.Content.ToString() == btnContent & btnTicTacToeGrid5.Content.ToString() == btnContent &
                 btnTicTacToeGrid6.Content.ToString() == btnContent)
               | (btnTicTacToeGrid7.Content.ToString() == btnContent & btnTicTacToeGrid8.Content.ToString() == btnContent &
                 btnTicTacToeGrid9.Content.ToString() == btnContent)
               | (btnTicTacToeGrid3.Content.ToString() == btnContent & btnTicTacToeGrid5.Content.ToString() == btnContent &
                 btnTicTacToeGrid7.Content.ToString() == btnContent))
            {
                if (btnContent == "O")
                {
                    MessageBox.Show("Spieler O hat Gewonnen!");
                    lblScoreBoardPlayerOScore.Content = ++i;
                }
                else if (btnContent == "X")
                {
                    MessageBox.Show("Spieler X hat Gewonnen!");
                    lblScoreBoardPlayerXScore.Content = ++j;
                }
                disablebuttons();
            }
            else
            {
                foreach (Button btn in wrapPanel.Children)
                {
                    if (btn.IsEnabled == true)
                    {
                        return;
                    }
                }
                MessageBox.Show("Spiel Vorbei! Keiner Gewinnt!");
            }
        }

        private void btnTicTacToeGrid1_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (turn == 1)
            {
                btn.Content = "O";
                lblCurrentPlayer.Content = "Aktuell am Zug: X";
            }
            else
            {
                btn.Content = "X";
                lblCurrentPlayer.Content = "Aktuell am Zug: O";
            }
            btn.IsEnabled = false;
            Win(btn.Content.ToString());
            turn += 1;
            if (turn > 2)
            {
                turn = 1;
            }
        }

        private void disablebuttons()
        {
            foreach (Button btn in wrapPanel.Children)
            {
                btn.IsEnabled = false;
            }
        }
    }
}
