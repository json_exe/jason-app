﻿using JasonsGameApp.CodeDateien;
using log4net;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für WalletSetupPage.xaml
    /// </summary>
    public partial class WalletSetupPage : Page
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public WalletSetupPage()
        {
            InitializeComponent();
            log.Info("Instanciating WalletSetupPage");
        }

        private void lblShowLicense_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            log.Debug("Showing License");
            MessageBox.Show("Nutzungsbedingungen fürs Wallet \n Sobald sie ein Wallet erstellen wird dies mit Ihrem Account Verknüpft. Sie können pro Account nur EIN Wallet besitzen." +
                " In Ihrem Wallet werden DevCoins gespeichert, mit denen sie in Zukunft ein paar Extra Features Freischalten können wie z.B.: Beta Keys. ES IST NICHT MÖGLICH DEVCOINS" +
                " DURCH ECHTES GELD ZU ERHALTEN! DevCoins werden grundsätzlich durch die Tasks, die sie unter Ihrem Wallet finden, erhalten. Darunter fallen: Wiederkehrende Tasks, " +
                "einmalige Tasks und Seasonale Tasks. In Zukunft wird es auch möglich sein DevCoins durch das spielen von Spielen zu erhalten. Sollten sie versuchen Ihren Wallet durch" +
                " unfaire Methoden, und weiteres, zu Verbessern werden wir Ihren Wallet zuerst Sperrren und/oder Löschen! Ein rückerhalt der bis dahin gesammelten DevCoins ist nicht " +
                "möglich. Hierbei handelt es sich um eine reine Fiktive Währung. Wenn sie ein Wallet erstellen speichern wir folgende Daten: Ihren Nutzernamen, Ihr Guthaben, Ihr Passwort" +
                " (Verschlüsselt) und ob sie ein Passwort anlegen wollen. Setzen sie den Haaken und erstellen ein Wallet erklären sie sich hiermit Einverstanden. Sie haben das Recht jederzeit" +
                " Ihre Daten anzufordern (Dabei senden wir KEINE Passwortdaten). Bei Fragen stehen wir Ihnen gerne zu Verfügung. Sie können uns auf dem Offiziellen Discord erreichen oder " +
                "Auf unserer Webseite: jasondevstudio.ddns.net (Noch im Bau). Ansonsten finden sie unsere E-Mail in dem About Bereich der Anwendung.");
        }

        private async void btnCreateWallet_Click(object sender, RoutedEventArgs e)
        {
            if (tBtnUsingPassword.IsChecked == true && txtBoxEnterWalletPassword.Text.Length < 8)
            {
                MessageBox.Show("Ihr Passwort ist zu kurz! Bitte geben sie ein anderes Passwort mit mindestens 8 Zeichen ein!");
                return;
            }
            DialogResult dialogResult = MessageBox.Show("Ihr Wallet wird jetzt erstellt. Dies kann einen moment dauern. Sobald Ihr Wallet erstellt wurde werden sie Automatisch" +
                " zu Ihrer Wallet seite gebracht. Klicken sie OK wenn sie Fortfahren möchten.", "Wallet Erstellung", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.Cancel)
            {
                return;
            }
            log.Info("Creating Wallet...");
            LoadingIndicator.Visibility = Visibility.Visible;
            btnCreateWallet.Visibility = Visibility.Hidden;
            txtBoxEnterWalletPassword.IsEnabled = false;
            checkBoxReadLicense.IsEnabled = false;
            rBtnUsingNoPassword.IsEnabled = false;
            tBtnUsingPassword.IsEnabled = false;
            UpdateLayout();
            WalletSetupCodes codes = new WalletSetupCodes();
            await codes.CreateWallet(txtBoxEnterWalletPassword.Text, NavigationService);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxReadLicense.IsChecked == true)
            {
                log.Debug("License accepted!");
                btnCreateWallet.IsEnabled = true;
            }
            else
            {
                log.Debug("License not accepted");
                btnCreateWallet.IsEnabled = false;
            }
        }

        private void rBtnUsingPassword_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Wallet will use Password");
            txtBoxEnterWalletPassword.Visibility = Visibility.Visible;
        }

        private void rBtnUsingNoPassword_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Wallet will use no Password");
            txtBoxEnterWalletPassword.Visibility = Visibility.Collapsed;
        }
    }
}
