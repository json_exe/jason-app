﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.UserControls;
using JasonsGameApp.Windows;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp
{
    /// <summary>
    /// Interaktionslogik für settings.xaml
    /// </summary>
    public partial class settings : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        NewMenu menu;
        bool ShowSplashScreen = Properties.Settings.Default.SplashScreen;
        public string OldUserName;

        public settings(NewMenu menu)
        {
            InitializeComponent();
            log.Info("Instanciated setting Page");
            this.menu = menu;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Deaktiviert und Aktiviert den Splash Screen dem man am Anfang 
            //vom Programm sieht
            if (ShowSplashScreen == true)
            {
                Properties.Settings.Default.SplashScreen = false;
                Properties.Settings.Default.Save();
                ShowSplashScreen = Properties.Settings.Default.SplashScreen;
                log.Debug("Splashscreen Tuned off");
                MessageBox.Show("SplashScreen Deaktiviert", "Settings Changed");
            }
            else
            {
                Properties.Settings.Default.SplashScreen = true;
                Properties.Settings.Default.Save();
                ShowSplashScreen = Properties.Settings.Default.SplashScreen;
                log.Debug("Splashscreen Tuned on");
                MessageBox.Show("SplashScreen Aktiviert", "Settings Changed");
            }
        }

        private void btnSetProgramKey_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) return;
            //Öffnet das Eingabefenster für die Keys
            if (runTimeSave.IsLoggedIn == false)
            {
                MessageBox.Show("Bitte Logge dich zuerst ein!");
                return;
            }
            EnterKeySettings enter = new EnterKeySettings();
            log.Info("Instanciated EnterKey Window");
            enter.Show();
        }

        private void btnChangeUsername_Click(object sender, RoutedEventArgs e)
        {
            //Öffnet das Fenster um seinen benutzernamen zu ändern
            if (runTimeSave.IsOnline == false) return;
            if (runTimeSave.IsLoggedIn == true)
            {
                OldUserName = runTimeSave.UserName;
                ChangeUsername changeUsername = new ChangeUsername(OldUserName, menu);
                log.Info("Instanciated ChangeUsername Window");
                changeUsername.Show();
            }
            else
            {
                MessageBox.Show("Bitte Logge dich zuerst ein, um dein Benutzernamen zu ändern!", "User Not Logged In");
            }
        }

        private void btnSelectStandardUpdateChannel_Click(object sender, RoutedEventArgs e)
        {
            //Öffnet das Fenster indem man sein Standard-Update Kanal auswählen kann
            Select_Update_Channel select = new Select_Update_Channel();
            log.Info("Instanciated SelectUpdateChannel Window");
            select.Show();
        }

        private void btnUnregisterRegisterFromMails_Click(object sender, RoutedEventArgs e)
        {
            //Meldet einen an/ab von den E-Mails
            if (runTimeSave.IsOnline == false) return;
            if (runTimeSave.IsLoggedIn == true)
            {
                SubscribeUnsubscribeMail mail = new SubscribeUnsubscribeMail();
                log.Info("Instanciated SubscribUnsubscribe Window");
                mail.Check();
            }
            else
            {
                MessageBox.Show("Bitte Logge dich zuerst ein, um dich von den E-Mails An- oder Abzumelden.", "User not Logged In");
                log.Debug("User not Logged in!");
            }
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            //Loggt einen Nutzer aus wenn dieser Angemeldet ist und deaktiviert die Funktion
            //Angemeldet bleiben
            if (runTimeSave.IsLoggedIn == true)
            {
                runTimeSave.IsLoggedIn = false;
                runTimeSave.UserName = "Not Logged in";
                menu.btnSubAccountManagement.Visibility = Visibility.Collapsed;
                menu.btnSubLoginSignUp.Visibility = Visibility.Visible;
                menu.txtBoxLoggedInUser.Text = runTimeSave.UserName;
                Properties.Settings.Default.AlwaysLoggedIn = false;
                Properties.Settings.Default.UserName = "";
                Properties.Settings.Default.Save();
                MessageBox.Show("Du wurdest Ausgeloggt!");
            }
            else
            {
                MessageBox.Show("Um dich abzumelden musst du dich erst Anmelden!");
            }
        }

        private void btnChangePassword_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == true && runTimeSave.IsLoggedIn == true)
            {
                ChangePassword changePassword = new ChangePassword();
                changePassword.Show();
            }
        }
    }
}
