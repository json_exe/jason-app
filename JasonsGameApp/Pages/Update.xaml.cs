﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.Windows;
using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für Update.xaml
    /// </summary>
    public partial class Update : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        public NewMenu menu;
        private MySqlConnection connection;
        private string NewVersionNumber, NewBetaVersion;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Info("SQL-Connection Established");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                log.Error("Cant Open Connection to MySQL Database: ", ex);
            }
        }

        private void CloseSqlConnection()
        {
            connection.Close();
            log.Debug("Connection Closed");
        }

        public Update(NewMenu menu)
        {
            InitializeComponent();
            log.Info("Instanciated Update Page");
            //Reading Current Version Number
            Version version = Assembly.GetEntryAssembly().GetName().Version;
            lblCurrentVersion.Content = version.ToString();
            //Erstellt einen WebClient
            this.menu = menu;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            string CurrentDir = Directory.GetCurrentDirectory();
            //Getting Changelog File
            try
            {
                if (runTimeSave.IsOnline == false) return;
                client.DownloadFileAsync(new Uri("https://www.dropbox.com/s/d4cakoo87sunsrp/changelog.txt?dl=1"), CurrentDir + "\\changelog.txt");
                client.DownloadFileCompleted += Client_ChangelogDownloaded;
                log.Info("Changelog Downloaded");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Download Changelog.", ex);
                MessageBox.Show("Fehler: Konnte Changelog nicht Herunterladen.", "Download Failed");
            }
        }

        private async void Client_ChangelogDownloaded(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            string CurrentDir = Directory.GetCurrentDirectory();
            if (File.Exists(CurrentDir + "\\changelog.txt"))
            {
                //Reading Changelog File
                StreamReader sr = new StreamReader(CurrentDir + @"\changelog.txt", Encoding.UTF8, true);
                txtBoxChangelog.Text = await sr.ReadToEndAsync();
                LoadingIndicator.Visibility = Visibility.Collapsed;
                log.Debug("Changelog Read");
            }
        }

        private void btnCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) return;
            //Guckt welcher Update Kanal ausgewählt ist und öffnet eine Datenbank Verbindung
            OpenSqlConnection();
            if (DropDownMenu.txtBoxSelection.Text == "Stable-Version")
            {
                try
                {   
                    //Holt die Versionsnummer von der Datenbank
                    string GetNewVersion = "SELECT AppVersion FROM AppSpecificInfo";
                    MySqlCommand cmd = new MySqlCommand(GetNewVersion, connection);
                    MySqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        NewVersionNumber = rd[0].ToString();
                    }
                    log.Info("Read NewVersion number");
                }
                catch (Exception ex)
                {
                    log.Error("Failed to Read new Version Number!", ex);
                    MessageBox.Show("Fehler beim Überprüfen der Version! Dies kann mehrere Ursachen haben: " +
                        "\n \n -Prüfen sie ob sie eine Internet Verbindung haben \n \n -Prüfen sie ob eventuell" +
                        " ein Antiviren Programm das Überprüfen verhindert. \n -Prüfen sie ob es auf den Offiziellen Discord" +
                        " eine Nachricht auf ausgefallene Server gab." +
                        "\n \n Sollten all diese Schritte nicht helfen, kontaktieren sie den Support und senden sie die neuste Log Datei mit!");
                    return;
                }
                finally
                {
                    CloseSqlConnection();
                }
                if (NewVersionNumber != string.Empty)
                {
                    log.Debug("VersionNumber Exist");
                    Version CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version;
                    Version NewVersion = Version.Parse(NewVersionNumber);
                    if (NewVersion > CurrentVersion)
                    {
                        log.Debug("Newer Version Available");
                        AllUpdateThigns codes = new AllUpdateThigns();
                        codes.WhatIsSelected(this);
                    }
                    else
                    {
                        log.Debug("Newest Version already Installed");
                        MessageBox.Show("Aktuellste Version ist bereits Installiert!");
                    }
                }
            }
            else if (DropDownMenu.txtBoxSelection.Text == "Beta-Version")
            {
                try
                {
                    //Holt die Beta-Versions Nummer aus der Datenbank
                    string GetNewVersion = "SELECT AppBetaVersion FROM AppSpecificInfo";
                    MySqlCommand cmd = new MySqlCommand(GetNewVersion, connection);
                    MySqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        NewBetaVersion = rd[0].ToString();
                    }
                    log.Info("Read NewBetaVersion number");
                }
                catch (Exception ex)
                {
                    log.Error("Failed to Read  BetaVersion Number!", ex);
                    MessageBox.Show("Fehler beim Überprüfen der Beta-Version! Dies kann mehrere Ursachen haben: " +
                        "\n \n -Prüfen sie ob sie eine Internet Verbindung haben \n \n -Prüfen sie ob eventuell" +
                        " ein Antiviren Programm das Überprüfen verhindert. \n -Prüfen sie ob es auf den Offiziellen Discord" +
                        " eine Nachricht auf ausgefallene Server gab." +
                        "\n \n Sollten all diese Schritte nicht helfen, kontaktieren sie den Support und senden sie die neuste Log Datei mit!");
                    return;
                }
                finally
                {
                    CloseSqlConnection();
                }
                if (NewBetaVersion != string.Empty)
                {
                    log.Debug("Beta Version exist");
                    Version CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version;
                    Version NewVersion = Version.Parse(NewBetaVersion);
                    if (NewVersion > CurrentVersion)
                    {
                        log.Debug("Newer Version Available");
                        if (runTimeSave.IsLoggedIn == true)
                        {
                            if (Properties.Settings.Default.ProgramKey != null)
                            {
                                log.Info("Read UserKey from Proram Settings");
                                AllUpdateThigns codes = new AllUpdateThigns();
                                codes.WhatIsSelected(this);
                            }
                            else
                            {
                                string Key = "empty";
                                log.Info("Reading UserKey from AccountInfo");
                                string CheckuserhasKey = "SELECT SavedKey FROM AccountInfo WHERE Username = @Username";
                                MySqlCommand cmd = new MySqlCommand(CheckuserhasKey, connection);
                                cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
                                MySqlDataReader reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    Key = (string)reader["SavedKey"];
                                }
                                if (Key != string.Empty && Key != "empty")
                                {
                                    AllUpdateThigns codes = new AllUpdateThigns();
                                    codes.WhatIsSelected(this);
                                }
                            }   
                        }
                        else
                        {
                            MessageBox.Show("Bitte stelle sicher das du Eingeloggt bist und verknüpfe zuerst einen Key mit deinem Profil indem du in den Einstellungen einen Key hinzufügst!");
                        }
                    }
                    else
                    {
                        log.Debug("Newest Version already Installed!");
                        MessageBox.Show("Keine Beta-Version vorhanden!");
                    }
                }
            }
            else if (DropDownMenu.txtBoxSelection.Text == "Alpha-Version")
            {
                MessageBox.Show("Aktuell ist keine Alpha-Version vorhanden!");
            }
            else
            {
                log.Error("Custom Error Code: U131 Cant recieve Update Channel");
                MessageBox.Show("Fehler beim Abruf des Update Kanals! Bitte überprüfen sie ob sie einen Kanal ausgewählt haben. Ansonsten wenden" +
                    " sie sich an den" +
                    " Support mit folgendem FehlerCode: U131 wenn dieses Problem weiterhin" +
                    " besteht. Senden sie ebenfalls die neuste Log Datei mit!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //Schließt die Datenbank Verbindung
            CloseSqlConnection();
        }
    }
}
