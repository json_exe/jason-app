﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;
using JasonsGameApp.CodeDateien;
using ToastNotifications.Messages;
using System.IO;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace JasonsGameApp.Pages.HomeMenu
{
    /// <summary>
    /// Interaktionslogik für Support.xaml
    /// </summary>
    public partial class Support : Page
    {
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        public Support()
        {
            InitializeComponent();
        }

        private async void btnSendSupportMessage_Click(object sender, RoutedEventArgs e)
        {
            Regex regexMail = new Regex(@"^[a-zA-Z0-9-_.+#,]{3,25}@\b(gmail|web|gmx|outlook|mail|yahoo)\b[.]\b(com|de)\b$");
            bool IsMail = regexMail.IsMatch(txtBoxUserMailAdress.Text);
            if (!IsMail)
            {
                runTimeSave.notifier.ShowWarning("Die E-Mail Adresse ist ungültig!");
                return;
            }
            DialogResult result = MessageBox.Show("Wollen sie Ihre Nachricht wirklich senden? Beachten sie: Beim senden werden Ihre " +
                "eingegebenen Daten sowie Ihr aktueller Nutzername an unseren Support weitergeleitet. Wir werden uns dann in nächster Zeit" +
                " bei Ihnen melden und Ihnen mitteilen inwiefern Ihre Anfrage bearbeitet wurde oder wird.", "Senden?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                LoadingIndicator.Visibility = Visibility.Visible;
                btnSendSupportMessage.Visibility = Visibility.Collapsed;
                await SupportCodes.SendSupportMessage(txtBoxUserMailAdress.Text, txtBoxSupportMessage.Text);
                LoadingIndicator.Visibility = Visibility.Collapsed;
                btnSendSupportMessage.Visibility = Visibility.Visible;
                txtBoxSupportMessage.Text = String.Empty;
                txtBoxUserMailAdress.Text = String.Empty;
            }
        }
    }

    internal class SupportCodes
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private static MySqlConnection connection;
        private static string AppKey = "CantRead";
        private static string UserRights = "CantRead";
        private static string UserName = "NotLoggedIn";

        internal static async Task SendSupportMessage(string UserMail, string UserMessage)
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            if (runTimeSave.IsLoggedIn == true)
            {
                try
                {
                    UserName = runTimeSave.UserName;
                    await connection.OpenAsync();
                    string GetAppKeyAndUserRights = "SELECT RoleName, SavedKey FROM AccountInfo INNER JOIN RolesTable WHERE AccountInfo.UserName = @UName AND RolesTable.id = AccountInfo.Roleid;";
                    MySqlCommand cmd = new MySqlCommand(GetAppKeyAndUserRights, connection);
                    cmd.Parameters.AddWithValue("@UName", runTimeSave.UserName);
                    MySqlDataReader rd = cmd.ExecuteReader();
                    while (await rd.ReadAsync())
                    {
                        if (rd["SavedKey"] != null)
                        {
                            AppKey = rd["SavedKey"].ToString();
                        }
                        UserRights = rd["RoleName"].ToString();
                    }
                    rd.Close();
                    await connection.CloseAsync();
                    log.Info("SQL-Connection Established and Data Successfully Read");
                }
                catch (Exception ex)
                {
                    log.Error("Cant Open Connection to MySQL Database: ", ex);
                }
            }
            try
            {
                WebClient webClient = new WebClient();
                //Erstellen einer neuen E-Mail Nachricht
                MailMessage message = new MailMessage();
                //Erstellen eines neuen Smtp Clients zum Versenden der Mail
                SmtpClient smtpClient = new SmtpClient();
                message.From = new MailAddress(UserMail);
                //Deklarieren wer der Empfänger ist
                message.To.Add(new MailAddress("jasondevelopmentstudio@gmail.com"));
                //Thema der Mail deklarieren.
                message.Subject = "Support Anfrage von: " + UserMail;
                //Deklarieren wie die Mail aussieht in HTML
                message.Body = $@"<h1 style='text-align: center;'><strong>Support Anfrage</strong></h1>
                                  <p style='text-align: center;'><strong> Eingegangene Support Anfrage von: {UserMail}</strong></p>
                                  <p> Informationen:</p>
                                  <ul>
                                    <li> Nutzername: {UserName}</li>
                                    <li> E - Mail Adresse: {UserMail}</li>
                                    <li> Berechtigungsstatus: {UserRights}</li>
                                    <li> App - Key: {AppKey}</li>
                                  </ul>
                                  <h2 style='text-align: center;'> Support - Nachricht </h2>
                                  <table style='border-collapse: collapse; width: 100%; height: 178px;border = 1;'>
                                    <tbody>
                                      <tr style='height: 178px;'>
                                        <td style='width: 100%; height: 178px; text-align: center;'>{UserMessage}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <p style='font-size: 10px; text-align: center;'> Diese Support - Nachricht wurde gesendet mit: App - Support Formular, am {DateTime.Now}</p>";
                //Text Encoder UTF8 mitgeben damit auch Buchstaben wie ß dargestellt werden
                message.SubjectEncoding = Encoding.UTF8;
                //Sagen das der E-Mail Inhalt in HTMl ist
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;
                //Port und Server für den smtp Client festlegen
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                //Anmeldeinformationen für den Client festlegen
                smtpClient.Credentials = new NetworkCredential("jasondevelopmentstudio@gmail.com", "purs xzzz ubtk evqj");
                //Mail senden
                await Task.Run(() => smtpClient.Send(message));
                runTimeSave.notifier.ShowSuccess("Support Anfrage wurde erfolgreich gesendet!");
                log.Info("Support Mail has been sended!");
            }
            catch (Exception ex)
            {
                //Falls fehler auftreten werden diese geloggt.
                runTimeSave.notifier.ShowError("Es ist ein Fehler aufgetreten. Bitte versuchen sie es erneut oder kontaktieren sie unseren" +
                    " Support anderweitig.");
                log.Error("Failed to Send Verify Mail", ex);
            }
        }

    }
}
