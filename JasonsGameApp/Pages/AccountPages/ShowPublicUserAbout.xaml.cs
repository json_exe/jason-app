﻿using JasonsGameApp.CodeDateien.UserProfileCodes;
using System.Windows.Controls;

namespace JasonsGameApp.Pages.AccountPages
{
    public partial class ShowPublicUserAbout : Page
    {

        private static PublicUserProfileCodes PublicSave = PublicUserProfileCodes.Instance;

        public ShowPublicUserAbout()
        {
            InitializeComponent();
            txtBoxAboutMe.Text = PublicSave.Description;
            this.UpdateLayout();
        }
    }
}
