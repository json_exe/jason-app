﻿using JasonsGameApp.CodeDateien.UserProfileCodes;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für ShowPublicUserData.xaml
    /// </summary>
    public partial class ShowPublicUserData : Page
    {

        public string Name = "";
        
        public ShowPublicUserData(string Name)
        {
            InitializeComponent();
            this.Name = Name;
        }

        public ShowPublicUserData()
        {
            InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UProfileCodes codes = new UProfileCodes();
            await codes.LoadUserData(this);
            LoadingGrid.Visibility = Visibility.Collapsed;
            AccountFrame.Visibility = Visibility.Visible;
            AccountFrame.Navigate(new ShowPublicUserInfo());
        }

        private void btnUserInformation_Click(object sender, RoutedEventArgs e)
        {
            AccountFrame.Navigate(new ShowPublicUserInfo());
        }

        private void btnUserAbout_Click(object sender, RoutedEventArgs e)
        {
            AccountFrame.Navigate(new ShowPublicUserAbout());
        }

        private void btnUserLikes_Click(object sender, RoutedEventArgs e)
        {
            AccountFrame.Navigate(new ShowPublicUserLikes());
        }
    }
}
