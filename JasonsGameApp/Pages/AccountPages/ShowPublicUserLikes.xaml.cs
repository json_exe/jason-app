﻿using JasonsGameApp.CodeDateien.UserProfileCodes;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für ShowPublicUserLikes.xaml
    /// </summary>
    public partial class ShowPublicUserLikes : Page
    {

        private static PublicUserProfileCodes PublicSave = PublicUserProfileCodes.Instance;

        public ShowPublicUserLikes()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (PublicSave.UserLikeList == null || PublicSave.UserLikeList.Count == 0)
            {
                return;
            }
            foreach (string item in PublicSave.UserLikeList)
            {
                listBoxAddedItems.Items.Add(item);
            }
        }
    }
}
