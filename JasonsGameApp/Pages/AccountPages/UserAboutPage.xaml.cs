﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.CodeDateien.UserProfileCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications.Messages;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für UserAboutPage.xaml
    /// </summary>
    public partial class UserAboutPage : Page
    {
        private static UserTempSave ProfileSave = UserTempSave.Instance;
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;


        public UserAboutPage()
        {
            InitializeComponent();
            txtBoxAboutMe.Text = ProfileSave.Description;
            this.UpdateLayout();
        }

        private async void ApplyAboutChanges_Click(object sender, RoutedEventArgs e)
        {
            if (txtBoxAboutMe.Text.Length < 1)
            {
                runTimeSave.notifier.ShowWarning("Es muss etwas eingegeben werden!");
                return;
            }
            LoadingIndicator.Visibility = Visibility.Visible;
            ApplyAboutChanges.Visibility = Visibility.Collapsed;
            UProfileCodes codes = new UProfileCodes();
            await codes.ApplyDescriptionChanges(this);
            ApplyAboutChanges.Visibility = Visibility.Visible;
            LoadingIndicator.Visibility = Visibility.Collapsed;
        }
    }
}
