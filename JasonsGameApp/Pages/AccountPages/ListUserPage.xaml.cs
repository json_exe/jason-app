﻿using JasonsGameApp.CodeDateien.UserProfileCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für ListUserPage.xaml
    /// </summary>
    public partial class ListUserPage : Page
    {
        public ListUserPage()
        {
            InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Task.Delay(1000);
            UProfileCodes codes = new UProfileCodes();
            await codes.LoadAllUsers(this);
            stackPanelLoading.Visibility = Visibility.Collapsed;
            stackPanelAllUsers.Visibility = Visibility.Visible;
        }
    }
}
