﻿using JasonsGameApp.CodeDateien.UserProfileCodes;
using System.Windows.Controls;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für ShowPublicUserInfo.xaml
    /// </summary>
    public partial class ShowPublicUserInfo : Page
    {

        private static PublicUserProfileCodes PublicSave = PublicUserProfileCodes.Instance;

        public ShowPublicUserInfo()
        {
            InitializeComponent();
            txtBoxName.Text = PublicSave.Name;
            txtBoxFullName.Text = PublicSave.FullName;
            txtBoxContact.Text = PublicSave.Contact;
            txtBoxLocation.Text = PublicSave.Location;
            txtBoxMobileNumber.Text = PublicSave.PhoneNumber;
            lblProfileFrom.Content = lblProfileFrom.Content + PublicSave.Name;
            this.UpdateLayout();
        }
    }
}
