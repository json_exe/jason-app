﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.CodeDateien.UserProfileCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications.Messages;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für UserInfoPage.xaml
    /// </summary>
    public partial class UserInfoPage : Page
    {
        private static UserTempSave ProfileSave = UserTempSave.Instance;
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;


        public UserInfoPage()
        {
            InitializeComponent();
            txtBoxName.Text = ProfileSave.Name;
            txtBoxFullName.Text = ProfileSave.FullName;
            txtBoxContact.Text = ProfileSave.Contact;
            txtBoxLocation.Text = ProfileSave.Location;
            txtBoxMobileNumber.Text = ProfileSave.PhoneNumber;
            this.UpdateLayout();
        }

        private async void btnApplyChanges_Click(object sender, RoutedEventArgs e)
        {
            if (txtBoxContact.Text.Length < 1 || txtBoxFullName.Text.Length < 1 || txtBoxLocation.Text.Length < 1 || txtBoxMobileNumber.Text.Length < 1 || txtBoxName.Text.Length < 1)
            {
                runTimeSave.notifier.ShowWarning("Alle Eingabefelder müssen befüllt werden!");
                return;
            }
            btnApplyChanges.Visibility = Visibility.Collapsed;
            LoadingIndicator.Visibility = Visibility.Visible;
            UProfileCodes codes = new UProfileCodes();
            await codes.ApplyInfoChanges(this);
            btnApplyChanges.Visibility = Visibility.Visible;
            LoadingIndicator.Visibility = Visibility.Collapsed;
        }
    }
}
