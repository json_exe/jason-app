﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.CodeDateien.UserProfileCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications.Messages;

namespace JasonsGameApp.Pages.AccountPages
{
    /// <summary>
    /// Interaktionslogik für UserLikesPage.xaml
    /// </summary>
    public partial class UserLikesPage : Page
    {
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private static UserTempSave ProfileSave = UserTempSave.Instance;


        public UserLikesPage()
        {
            InitializeComponent();
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            if (UserInput.Text.Length < 3)
            {
                runTimeSave.notifier.ShowWarning("Die Eingabe muss länger als 3 Zeichen sein!");
                return;
            }
            listBoxAddedItems.Items.Add(UserInput.Text);
            UserInput.Text = String.Empty;
        }

        private void btnRemoveItem_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxAddedItems.SelectedItem != null)
            {
                listBoxAddedItems.Items.Remove(listBoxAddedItems.SelectedItem);
            }
            else
            {
                runTimeSave.notifier.ShowWarning("Bitte wähle vorher einen Eintrag aus der Liste");
            }
        }

        private async void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxAddedItems.Items.Count < 1)
            {
                runTimeSave.notifier.ShowWarning("Es muss mindestens 1 Element der Liste hinnzugefügt werden!");
                return;
            }
            LoadingIndicator.Visibility = Visibility.Visible;
            btnSaveChanges.Visibility = Visibility.Collapsed;
            UProfileCodes codes = new UProfileCodes();
            await codes.ApplyListChanges(this);
            LoadingIndicator.Visibility = Visibility.Collapsed;
            btnSaveChanges.Visibility = Visibility.Visible;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (ProfileSave.UserLikeList == null || ProfileSave.UserLikeList.Count == 0)
            {
                return;
            }
            foreach (string item in ProfileSave.UserLikeList)
            {
                listBoxAddedItems.Items.Add(item);
            }
        }
    }
}
