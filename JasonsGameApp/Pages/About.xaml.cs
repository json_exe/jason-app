﻿using JasonsGameApp.CodeDateien;
using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für About.xaml
    /// </summary>
    public partial class About : Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private MySqlConnection connection;
        private string NewVersionNumber;
        private bool failed = false;

        public About()
        {
            InitializeComponent();
            log.Info("Instanciated About Page");
        }

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Info("SQL-Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                failed = true;
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie die neuste Log Datei mit!");
            }
        }

        private void CloseSqlConnection()
        {
            connection.Close();
            log.Debug("Connection Closed");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) failed = true;
            //Baut eine Datenbank Verbindung auf.
            OpenSqlConnection();

            //Versucht Daten von der Datenbank zu holen
            if (failed == false)
            {

                try
                {
                    string GetNewVersion = "SELECT AppVersion FROM AppSpecificInfo";
                    MySqlCommand cmd = new MySqlCommand(GetNewVersion, connection);
                    MySqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        NewVersionNumber = rd[0].ToString();
                    }
                    log.Info("Read NewVersion number");
                }
                catch (Exception ex)
                {
                    failed = true;
                    log.Error("Failed to Read new Version Number!", ex);
                    MessageBox.Show("Fehler beim Überprüfen der Version! Dies kann mehrere Ursachen haben: " +
                        "\n \n -Prüfen sie ob sie eine Internet Verbindung haben \n \n -Prüfen sie ob eventuell" +
                        " ein Antiviren Programm das Überprüfen verhindert. \n -Prüfen sie ob es auf den Offiziellen Discord" +
                        " eine Nachricht auf ausgefallene Server gab." +
                        "\n \n Sollten all diese Schritte nicht helfen, kontaktieren sie den Support und senden sie die neuste Log Datei mit!");
                }

                //Schließt die Datenbank Verbindung
                CloseSqlConnection();

            }

            //Holt sich die aktuelle Version des Programms aus der AssemblyInfo.cs Datei
            Version version = Assembly.GetEntryAssembly().GetName().Version;

            //Erstellt eine neue Version und setzt diese VORERST gleich mit der Aktuellen
            Version NewVersion = version;

            //Prüft ob es bei der Datenbank Verbindung Probleme gab
            if (failed == false)
            {
                //Sollte es keine Fehler geben wird die von der Datenbank gelesene Versionsnummer genommen
                NewVersion = Version.Parse(NewVersionNumber);
            }

            log.Debug("Read Version Number and Saved");

            //Guckt ob die Version Aktuell ist.
            if (NewVersion > version)
            {
                log.Info("Newer Version Available");
                AppVersion.Content = "AppVersion: " + version + " (Neuere Version erhältlich > " + NewVersion + ")";
            }
            else
            {
                log.Debug("No new Version Available");
                AppVersion.Content = "AppVersion: " + version + " (Aktuellste Version)";
            }
        }
    }
}
