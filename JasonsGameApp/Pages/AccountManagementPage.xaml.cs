﻿using JasonsGameApp.CodeDateien.UserProfileCodes;
using JasonsGameApp.Pages.AccountPages;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.Pages
{
    /// <summary>
    /// Interaktionslogik für AccountManagementPage.xaml
    /// </summary>
    public partial class AccountManagementPage : Page
    {

        private static UserTempSave ProfileSave = UserTempSave.Instance;

        public AccountManagementPage()
        {
            InitializeComponent();
        }

        private void btnUserInformation_Click(object sender, RoutedEventArgs e)
        {
            AccountFrame.Navigate(new UserInfoPage());
        }

        private void btnUserAbout_Click(object sender, RoutedEventArgs e)
        {
            AccountFrame.Navigate(new UserAboutPage());
        }

        private void btnUserLikes_Click(object sender, RoutedEventArgs e)
        {
            AccountFrame.Navigate(new UserLikesPage());
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (ProfileSave.InitialLoad == true)
            {
                UProfileCodes codes = new UProfileCodes();
                await codes.LoadInitialData(this);
                AccountFrame.Navigate(new UserInfoPage());
            }
            else
            {
                LoadingGrid.Visibility = Visibility.Hidden;
                LoadingIndicator.Visibility = Visibility.Hidden;
                AccountFrame.Visibility = Visibility.Visible;
                AccountFrame.Navigate(new UserInfoPage());
            }
        }

        private async void btnDeleteProfile_Click(object sender, RoutedEventArgs e)
        {
            DialogResult result = MessageBox.Show("Möchten sie wirklich Ihr Profil Löschen? Dies wird alle gespeicherten Informationen entfernen.", "Profile-Daten löschen?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                UProfileCodes codes = new UProfileCodes();
                await codes.DeleteUserProfile(this);
            }
        }
    }
}
