﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.Windows;
using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace JasonsGameApp
{
    public class SignUpCodes
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        int VerificationCode;
        bool failed = false;
        MySqlConnection connection;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Info("SQL-Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                failed = true;
            }
        }

        private void CloseSqlConnection()
        {
            connection.Close();
            log.Debug("Connection Closed");
        }

        public void RegisterNewUser(SignUp signUp)
        {
            OpenSqlConnection();
            if (failed == true) return;
            //Erstellen der gesammten Überprüfungen ob die Eingegebenen Daten stimmen.
            Regex regexMail = new Regex(@"^[a-zA-Z0-9-_.+#,]{3,25}@\b(gmail|web|gmx|outlook|mail|yahoo)\b[.]\b(com|de)\b$");
            Regex regexPswd = new Regex("^[a-zA-Z0-9-_!#<>+]{8,50}$");
            Regex regexUserName = new Regex("^[a-zA-Z0-9-_.#<>]{3,25}$");
            Regex VerifyCode = new Regex("^[0-9]{6}$");
            bool IsEmail = regexMail.IsMatch(signUp.txtEMail.Text);
            bool IsPassword = regexPswd.IsMatch(signUp.EnterPSWD.Password);
            bool IsVerificationCode = VerifyCode.IsMatch(signUp.txtVerifyCode.Text);
            bool IsUserName = regexUserName.IsMatch(signUp.txtUsername.Text);

            if (signUp.txtVerifyCode.Visibility == Visibility.Hidden)
            {

                if (IsEmail == true)
                {
                    //Datenbankcheck, ob E-Mail bereits Registriert und Verifiziert!
                    //Checken ob E-Mail bereits Registiert ist.
                    string queryMail = "SELECT * FROM AccountInfo WHERE EMail = @EMail";
                    MySqlCommand cmd = new MySqlCommand(queryMail, connection);
                    cmd.Parameters.AddWithValue("@EMail", signUp.txtEMail.Text);
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    log.Debug("Executinr MySQL Data Reader");
                    if (dataReader.Read())
                    {
                        dataReader.Close();
                        log.Debug("Closing MySQL Data Reader");
                        string queryEmailVerified = "SELECT * FROM AccountInfo WHERE EMail = @EMail and Verificated = 1";
                        MySqlCommand cmd4 = new MySqlCommand(queryEmailVerified, connection);
                        cmd4.Parameters.AddWithValue("@EMail", signUp.txtEMail.Text);
                        MySqlDataReader IsVerified = cmd4.ExecuteReader();
                        log.Debug("Opening IsVerified Reader");
                        if (IsVerified.Read())
                        {
                            MessageBox.Show("E-Mail Adresse ist bereits Registriert und Verifiziert! Bitte begebe dich zum Login und Logge dich ein!");
                            log.Info("E-Mail is Already Registratred");
                        }
                        else
                        {
                            Random rndm = new Random();
                            int Code = rndm.Next(100000, 999999);
                            VerificationCode = Code;
                            try
                            {
                                VerfyMail(Code, signUp.txtEMail.Text);
                            }
                            catch (Exception ex)
                            {
                                log.Error("Failed to send Verify Mail", ex);
                                Console.WriteLine("Es gab einen Fehler beim senden der Verifizierungsmail. Bitte überprüfen sie: \n -Das sie eine Internet Verbindung haben \n -Versuchen sie" +
                                    " es erneut. \n Sollte das Problem weiterhin bestehen, melden sie sich bitte beim Support mit der neusten Log Datei.");
                                return;
                            }
                            log.Info("Sended Verification Mail");
                            signUp.txtVerifyCode.Visibility = Visibility.Visible;
                            MessageBox.Show("E-Mail Adresse ist im System vorhanden, jedoch nicht Verifiziert. Wir haben dir " +
                                "nun einen neuen Verifizierungscode gesendet. Gebe diesen bitte in das neue Textfeld ein und " +
                                "drücke erneut auf SignUp!");
                        }
                    }
                    else
                    {

                        if (IsUserName == true)
                        {
                            dataReader.Close();
                            log.Debug("Closing Reader");
                            string queryUserName = "SELECT * FROM AccountInfo WHERE UserName = @UserName";
                            MySqlCommand cmd2 = new MySqlCommand(queryUserName, connection);
                            cmd2.Parameters.AddWithValue("@UserName", signUp.txtUsername.Text);
                            MySqlDataReader readUserName = cmd2.ExecuteReader();
                            log.Debug("Executing readUserName Reader");
                            if (readUserName.Read())
                            {
                                MessageBox.Show("Benutzername ist bereits vergeben. Bitte wähle einen anderen Benutzernamen!",
                                    "Username Existing");
                                log.Debug("UserName Exists");

                            }
                            else
                            {

                                if (IsPassword == true)
                                {
                                    signUp.txtVerifyCode.Visibility = Visibility.Visible;
                                    Random rndm = new Random();
                                    int Code = rndm.Next(100000, 999999);
                                    VerificationCode = Code;
                                    readUserName.Close();
                                    log.Debug("Closing Reader");
                                    string queryAddNewUser = "INSERT INTO AccountInfo (EMail, Password, Verificated, UserName, RecieveMail) VALUES (@EMail,@Password,0,@UserName,1)";
                                    MySqlCommand cmd3 = new MySqlCommand(queryAddNewUser, connection);
                                    cmd3.Parameters.AddWithValue("@EMail", signUp.txtEMail.Text);
                                    cmd3.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(signUp.EnterPSWD.Password));
                                    cmd3.Parameters.AddWithValue("@UserName", signUp.txtUsername.Text);
                                    try
                                    {
                                        cmd3.ExecuteScalar();
                                        log.Debug("Successfully Executed MySQL-Command");
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show("Failed to Add User! Please check: \n -That you have a Inet Connection \n -That on the App Discord" +
                                            " that the Server isnt Down at the moment \n -Retry a second Time \n Otherwise please contact Support " +
                                            "with the newest Log File!", "Exception Occured", MessageBoxButton.OK, MessageBoxImage.Error);
                                        log.Error("Failed to Execute SQL-Command with query AddNewUser", ex);
                                        return;
                                    }
                                    VerfyMail(Code, signUp.txtEMail.Text);
                                    log.Debug("Verify Mail sended");
                                    MessageBox.Show("Registrierung fast Abgeschlossen! \n \n Wir haben Ihnen nun eine E-Mail geschickt " +
                                        "in der sich ein Verifizierungscode Befindet. Bitte geben sie den Code in das neue Textfeld " +
                                        "ein und klicken sie erneut auf SignUp!", "Verfy Step");

                                }
                                else
                                {
                                    MessageBox.Show("Bitte gebe ein Gültiges Passwort ein, das unseren Kriterien entspricht! \n \n Passwort Kriterien: " +
                                        "\n -Mindestens 8 Zeichen \n -Keine Sonderzeichen außer; - _ ! # < > +",
                                        "Password Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                    log.Debug("UserPassword Incorrect");
                                }

                            }

                        }
                        else
                        {
                            MessageBox.Show("Bitte gebe einen Gültigen Benutzernamen ein! Der Benutzername darf nur " +
                                "zwischen 3 und 25 Zeichen enthalten und darf nur Folgende Sonderzeichen enthalten: \n " +
                                "- _ . # < >", "Username not Right");
                            log.Debug("UserName Incorrect");
                        }

                    }

                }
                else
                {
                    MessageBox.Show("Bitte gebe eine Richtige E-Mail Adresse ein! E-Mail Adresse muss gmail, outlook, yahoo, gmx, web oder mail sein!" +
                        " Weitere E-Mail Adressen werden in Zukunft hinzugefügt!", "Wrong Mail");
                    log.Debug("User Mail Incorrect");
                }

            }
            else
            {

                if (IsVerificationCode == true)
                {

                    if (signUp.txtVerifyCode.Text == VerificationCode.ToString())
                    {
                        string queryVerifyUser = "UPDATE AccountInfo SET Verificated = 1 WHERE EMail = @EMail";
                        MySqlCommand cmd5 = new MySqlCommand(queryVerifyUser, connection);
                        cmd5.Parameters.AddWithValue("@EMail", signUp.txtEMail.Text);
                        try
                        {
                            cmd5.ExecuteNonQuery();
                            log.Debug("Query Executed Successfully");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Failed to Verify User! Please Check: \n -That you have an Inet Connection \n -On the Discord the Server status" +
                                " isnt Down \n -Please retry a second time \n Otherwise please contact Support with the newest Log File!");
                            log.Error("Failed to Execute SQL Command with query VerifyUser", ex);
                        }
                        EmailAfterRegist(signUp.txtEMail.Text);
                        log.Debug("AfterRegist Mail successfully sended");
                        MessageBox.Show("Account Registriert und Verifiziert! Nun kannst du dich beim Login mit deinen Daten" +
                            " Anmelden!");
                        Login login = new Login();
                        log.Debug("Instanciated Login Window");
                        login.Show();
                        log.Info("Showing Login");
                        signUp.Close();
                        log.Debug("Closing SignUp");
                    }
                    else
                    {
                        MessageBox.Show("Der Code stimmt nicht überein! Bitte überprüfe deine Eingabe erneut.");
                        log.Debug("Verify Code Incorrect");
                    }
                }
                else
                {
                    MessageBox.Show("Bitte gebe einen gültigen Verifizierungscode ein! Der Code hat exakt 6 Nummern.", "Verify Code Wrong", 
                        MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    log.Debug("Verify Code Incorrect");
                }
            }

            CloseSqlConnection();
            log.Debug("Closing SQL-Connection");
        }

        private void VerfyMail(int VerifyCode, string EMail)
        {
            try
            {
                WebClient webClient = new WebClient();
                //Erstellen einer neuen E-Mail Nachricht
                MailMessage message = new MailMessage();
                //Erstellen eines neuen Smtp Clients zum Versenden der Mail
                SmtpClient smtpClient = new SmtpClient();
                message.From = new MailAddress("jasondevelopmentstudio@gmail.com");
                //Deklarieren wer der Empfänger ist
                message.To.Add(new MailAddress(EMail));
                //Thema der Mail deklarieren.
                message.Subject = "Registrierung! VerifyCode: " + VerifyCode;
                //HTML-Code der E-Mail aus Date in der Cloud auslesen und richtig Encoden.
                var htmlData = webClient.DownloadData("https://jsprivatenextcloud.ddns.net/index.php/s/x9ecrXqbmWqHbG3/download/emailwithcode.html");
                var htmlCode = Encoding.UTF8.GetString(htmlData);
                //Deklarieren wie die Mail aussieht in HTML
                message.Body = htmlCode;
                //Text Encoder UTF8 mitgeben damit auch Buchstaben wie ß dargestellt werden
                message.SubjectEncoding = Encoding.UTF8;
                //Sagen das der E-Mail Inhalt in HTMl ist
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;
                //Port und Server für den smtp Client festlegen
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                //Anmeldeinformationen für den Client festlegen
                smtpClient.Credentials = new NetworkCredential("jasondevelopmentstudio@gmail.com", "purs xzzz ubtk evqj");
                //Mail senden
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                //Falls fehler auftreten werden diese in einer TextBox ausgegeben
                MessageBox.Show("Failed to send the Verify Mail. Please check: \n -That you have an Inet Connection \n -That youre E-Mail is right " +
                    "\n Otherwise please contact Support with the newest Log File!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                log.Error("Failed to Send Verify Mail", ex);
            }
        }

        private string PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Encrypting Password");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Password Encrypted");
                return builder.ToString();
            }
        }

        private void EmailAfterRegist(string EMail)
        {
            try
            {
                WebClient webClient = new WebClient();
                //Erstellen einer neuen Message
                MailMessage message = new MailMessage();
                //Erstellen eines neuen Clients
                SmtpClient smtpClient = new SmtpClient();
                message.From = new MailAddress("jasondevelopmentstudio@gmail.com");
                //Empfänger der E-Mail deklarieren
                message.To.Add(new MailAddress(EMail));
                //Thema der E-Mail deklarieren
                message.Subject = "Willkommen in der Community " + EMail;
                var htmlData = webClient.DownloadData("https://jsprivatenextcloud.ddns.net/index.php/s/C932f2fNefFg9BB/download/userregistmail.html");
                var htmlCode = Encoding.UTF8.GetString(htmlData);
                //Inhalt der E-Mail als HTML deklarieren
                message.Body = htmlCode;
                //Encoding option UTF8 benutzen
                message.SubjectEncoding = Encoding.UTF8;
                //Sagen das der Inhalt der Mail HTML ist
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Low;
                //Festlegen von Port und Host des Clients
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                //Anmeldedaten bereitstellen
                smtpClient.Credentials = new NetworkCredential("jasondevelopmentstudio@gmail.com", "purs xzzz ubtk evqj");
                //E-Mail senden
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                //Ausgeben des Fehlers in einer TextBox
                MessageBox.Show("Failed to send the AfterRegist Mail! Please check: \n -That you have an Inet Connection \n -That youre E-Mail " +
                    "is right \n Otherwise please contact Support with the latest Log Files!", "ERROR SENDING MAIL", MessageBoxButton.OK, MessageBoxImage.Error);
                log.Error("Failed to send AfterRegist Mail.", ex);
            }
        }
    }
}
