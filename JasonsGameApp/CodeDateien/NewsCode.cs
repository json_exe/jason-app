﻿using System;
using System.Threading.Tasks;
using JasonsGameApp.Pages;
using MySql.Data.MySqlClient;
using System.Windows.Media;
using System.Windows;
using System.IO;

namespace JasonsGameApp.CodeDateien
{
    public class NewsCode
    {
        //WelcomePage Elemente überall im Code nutzen
        private WelcomePage page;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        //erstellen einer neuen MySqlConnection
        private MySqlConnection connection;
        private string NewsBox1, NewsBox2, NewsBox3;
        private bool NewsFailed = false;
        bool ConFail = false;

        public async void InitNews(WelcomePage page)
        {
            this.page = page;
            CheckConnection conCheck = new CheckConnection();
            runTimeSave.IsOnline = await Task.Run(() => conCheck.RequestAsync());
            if (runTimeSave.IsOnline == true)
            {
                page.menu.lblConStatus.Content = "Status: Online";
                page.menu.lblConStatus.Foreground = Brushes.Green;
                if (runTimeSave.IsLoggedIn != false)
                {
                    runTimeSave.RetrieveUserID();
                }
            }
            else
            {
                page.menu.lblConStatus.Content = "Status: Offline";
                page.menu.lblConStatus.Foreground = Brushes.Red;
                MessageBox.Show("Es konnte keine Verbindung zum App-Server hergestellt werden. Die App wechselt in" +
                    " den Offline Modus. Einige Features der App sind hierdurch nicht oder nur noch eingeschränkt" +
                    " nutzbar! Um erneut eine Verbindung herzustellen, starte bitte die App neu.", "Keine Verbindung",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            page.menu.UpdateLayout();
            RetrievingNews();
        }

        public async void RetrieveWalletState()
        {
            await OpenSqlConnection();
            string WalletStatus = "SELECT IsUsingWallet FROM WalletSystem WHERE UserName = @Username";
            MySqlCommand cmd = new MySqlCommand(WalletStatus, connection);
            cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
            MySqlDataReader reader = cmd.ExecuteReader();
            int State = 0;
            if (reader.Read())
            {
                State = (int)reader["IsUsingWallet"];
                if (State == 1)
                {
                    Properties.Settings.Default.WalletInUse = true;
                }
                else
                {
                    Properties.Settings.Default.WalletInUse = false;
                }
            }
            else
            {
                Properties.Settings.Default.WalletInUse = false;
            }
            connection.Close();
            Properties.Settings.Default.Save();
        }

        private async Task OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                await connection.OpenAsync();
            }
            catch (Exception ex)
            {
                log.Error("Failed to Connect to Database!", ex);
                ConFail = true;
            }
        }

        private async void RetrievingNews()
        {

            //Prüft ob die News schonmal geholt wurden (ONLY PER RUNTIME) und ob es schon vorher einen fehler gab
            if (runTimeSave.RetrievedNews == false && runTimeSave.IsOnline == true)
            {
                runTimeSave.RetrievedNews = true;
                //Baut eine Verbindung zur Datenbank auf
                await Task.Run(() => OpenSqlConnection());

                if (ConFail == false)
                {
                    //Abrufen der einzelnen News Daten aus der Datenbank durch SQL-Querrys und CMDs
                    string queryCallNews1 = "SELECT News1 FROM News";
                    string queryCallNews2 = "SELECT News2 FROM News";
                    string queryCallNews3 = "SELECT News3 FROM News";
                    MySqlCommand cmd1 = new MySqlCommand(queryCallNews1, connection);
                    MySqlCommand cmd2 = new MySqlCommand(queryCallNews2, connection);
                    MySqlCommand cmd3 = new MySqlCommand(queryCallNews3, connection);
                    log.Debug("Builded CN-Strings and SQL-Commands");
                    try
                    {
                        //Ausführen aller DataReader und deren Ergebnisse in die Strings speichern
                        MySqlDataReader rd = (MySqlDataReader)await cmd1.ExecuteReaderAsync();
                        while (rd.Read())
                        {
                            NewsBox1 = rd[0].ToString();
                        }
                        rd.Close();
                        MySqlDataReader rd2 = (MySqlDataReader)await cmd2.ExecuteReaderAsync();
                        while (rd2.Read())
                        {
                            NewsBox2 = rd2[0].ToString();
                        }
                        rd2.Close();
                        MySqlDataReader rd3 = (MySqlDataReader)await cmd3.ExecuteReaderAsync();
                        while (rd3.Read())
                        {
                            NewsBox3 = rd3[0].ToString();
                        }
                        rd3.Close();
                        log.Info("Retrieved News.");
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to retrieve News! Exception: ", ex);
                        NewsFailed = true;
                    }
                    //Schließt die Datenbank Verbindung
                    connection.Close();
                }

                //Daten wurden nun gelesen und in den Strings gespeichert (Sofern es keinen Fehler gab).
                //Gelesene Daten müssen nun, sofern es keinen fehler gab, in die Textboxxen geladen werden.
                //Prüft on es vorher fehler gab
                if (NewsFailed == false && ConFail == false)
                {
                    //Lädt gespeicherte Texte in die TextBoxxen und Speichert die neuen news in den Anwendungs Einstellungen ab.
                    log.Info("Loaded Database news and Saved PreSave News");
                    page.txtBoxNews1.Text = NewsBox1;
                    page.txtBoxNews2.Text = NewsBox2;
                    page.txtBoxNews3.Text = NewsBox3;
                    Properties.Settings.Default.PreSavedNews1 = NewsBox1;
                    Properties.Settings.Default.PreSavedNews2 = NewsBox2;
                    Properties.Settings.Default.PreSavedNews3 = NewsBox3;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    //Sollte es zu Problemen kommen (Keine Verbindung oder Fehler beim auslesen)
                    //Lade die zuletzt gespeicherten News aus den Settings.
                    log.Info("Loaded PreSaved news");
                    page.txtBoxNews1.Text = Properties.Settings.Default.PreSavedNews1;
                    page.txtBoxNews2.Text = Properties.Settings.Default.PreSavedNews2;
                    page.txtBoxNews3.Text = Properties.Settings.Default.PreSavedNews3;
                }

            }
            else
            {
                //Sollten die News bereits einmal geladen worden sein, lade bitte aus den Settings
                log.Info("Already Retrieved News. Loading PreSaved");
                page.txtBoxNews1.Text = Properties.Settings.Default.PreSavedNews1;
                page.txtBoxNews2.Text = Properties.Settings.Default.PreSavedNews2;
                page.txtBoxNews3.Text = Properties.Settings.Default.PreSavedNews3;
            }
            //Refresht den inhalt der Seite
            page.UpdateLayout();
        }

    }
}
