﻿using log4net;
using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text;
using JasonsGameApp.Pages;
using ToastNotifications.Messages;
using System.Windows.Media;

namespace JasonsGameApp.CodeDateien
{
    public class UserWalletCodes
    {
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private MySqlConnection connection;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


        public bool CheckWalletSetting()
        {
            OpenSqlConnection();
            string CheckSecure = "SELECT IsUsingWalletPassword FROM WalletSystem WHERE UserName = @Username";
            MySqlCommand cmd = new MySqlCommand(CheckSecure, connection);
            cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
            MySqlDataReader reader = cmd.ExecuteReader();
            int check = 0;
            while (reader.Read())
            {
                check = (int)reader["IsUsingWalletPassword"];
            }
            if (check == 1)
            {
                connection.Close();
                return true;
            }
            else
            {
                connection.Close();
                return false;
            }
        }

        public int LoadWalletCount()
        {
            OpenSqlConnection();
            string LoadWalletCount = "SELECT WalletCount FROM WalletSystem WHERE UserName = @Username";
            MySqlCommand cmd = new MySqlCommand(LoadWalletCount, connection);
            cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                return (int)reader["WalletCount"];
            }
            connection.Close();
            return 0;   
        }

        public bool UnlockWallet(string Password, UserWallet wallet)
        {
            OpenSqlConnection();
            string UnlockWallet = "SELECT WalletPassword FROM WalletSystem WHERE UserName = @Username AND WalletPassword = @Password";
            MySqlCommand cmd = new MySqlCommand(UnlockWallet, connection);
            cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
            cmd.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(Password));
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                runTimeSave.notifier.ShowSuccess("Wallet Entsperrt!");
                Properties.Settings.Default.WalletUnlocked = true;
                Properties.Settings.Default.Save();
                wallet.SecureWalletDisplay.Visibility = System.Windows.Visibility.Hidden;
                wallet.MainWalletDisplay.Visibility = System.Windows.Visibility.Visible;
                return true;
            }
            else
            {
                runTimeSave.notifier.ShowWarning("Wallet konnte nicht entsperrt werden");
                wallet.pswdBoxEnterWalletPassword.BorderBrush = Brushes.Red;
                return false;
            }
        }

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
            }
        }

        private string PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Encrypting Password");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Password Encrypted");
                return builder.ToString();
            }
        }


    }
}
