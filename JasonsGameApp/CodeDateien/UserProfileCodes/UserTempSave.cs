﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonsGameApp.CodeDateien.UserProfileCodes
{
    public class UserTempSave
    {
        private static UserTempSave instance = null;
        private static readonly object padlock = new object();

        public string Name;
        public string FullName;
        public string Description;
        public string Contact;
        public string PhoneNumber;
        public string Location;
        public bool InitialLoad = true;
        public List<string> UserLikeList = new List<string>();

        private UserTempSave()
        {

        }

        public static UserTempSave Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new UserTempSave();
                    }
                    return instance;
                }
            }
        }

    }
}
