﻿using System.Collections.Generic;

namespace JasonsGameApp.CodeDateien.UserProfileCodes
{
    public class PublicUserProfileCodes
    {

        private static PublicUserProfileCodes instance = null;
        private static readonly object padlock = new object();

        public string Name;
        public string FullName;
        public string Description;
        public string Contact;
        public string PhoneNumber;
        public string Location;
        public bool InitialLoad = true;
        public List<string> UserLikeList = new List<string>();

        private PublicUserProfileCodes()
        {

        }

        public static PublicUserProfileCodes Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new PublicUserProfileCodes();
                    }
                    return instance;
                }
            }
        }

    }
}
