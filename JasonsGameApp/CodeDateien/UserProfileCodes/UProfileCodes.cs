﻿using JasonsGameApp.Pages;
using JasonsGameApp.Pages.AccountPages;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ToastNotifications.Messages;

namespace JasonsGameApp.CodeDateien.UserProfileCodes
{
    public class UProfileCodes
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private static UserTempSave ProfileSave = UserTempSave.Instance;
        private static PublicUserProfileCodes PublicSave = PublicUserProfileCodes.Instance;
        private MySqlConnection connection;

        private async Task<bool> OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                await connection.OpenAsync();
                log.Debug("Connection Established");
                return true;
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                runTimeSave.notifier.ShowError("Fehler beim Verbinden zur Datenbank");
                runTimeSave.IsOnline = false;
                runTimeSave.menu.UpdateLayout();
                return false;
            }
        }

        public async Task LoadInitialData(AccountManagementPage page)
        {
            await Task.Delay(1300);
            if (!ProfileSave.InitialLoad) return;
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel Profile Loading");
                page.LoadingIndicator.Visibility = System.Windows.Visibility.Hidden;
                page.WarnLabel.Visibility = System.Windows.Visibility.Visible;
                page.WarnLabel.Content = "Daten konnten nicht geladen werden";
                return;
            }
            string CheckIfUserProfileExists = "SELECT uid FROM AccountInfo INNER JOIN ProfileInformation ON AccountInfo.id = ProfileInformation.uid WHERE UserName = @UName";
            MySqlCommand cmd = new MySqlCommand(CheckIfUserProfileExists, connection);
            cmd.Parameters.AddWithValue("@UName", runTimeSave.UserName);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (!await reader.ReadAsync())
            {
                log.Info("User Profile doesnt exists. Creating one...");
                reader.Close();
                string ImportUserProfileToDatabase = "INSERT INTO ProfileInformation (uid) VALUES (@UID)";
                MySqlCommand cmd2 = new MySqlCommand(ImportUserProfileToDatabase, connection);
                cmd2.Parameters.AddWithValue("@UID", runTimeSave.UID);
                try
                {
                    await cmd2.ExecuteScalarAsync();
                    log.Info("Successfully Inserted User to Profile Database");
                }
                catch (Exception ex)
                {
                    log.Fatal($"An Error Occured while Inserting User to ProfileDatabase with query: " +
                        $"{ImportUserProfileToDatabase}. Exception: ", ex);
                    runTimeSave.notifier.ShowError("FatalError: Unbekannter Fehler bei erstellung des NutzerProfils");
                    page.LoadingIndicator.Visibility = System.Windows.Visibility.Hidden;
                    page.WarnLabel.Visibility = System.Windows.Visibility.Visible;
                    page.WarnLabel.Content = "Kritischer Fehler";
                    runTimeSave.IsOnline = false;
                    runTimeSave.menu.UpdateLayout();
                    await connection.CloseAsync();
                    return;
                }
            }
            reader.Close();
            string ListAsString = string.Empty;
            string LoadSavedProfileData = "SELECT Description, Name, Fullname, Location, PhoneNumber, Contact, LikeList FROM AccountInfo INNER JOIN ProfileInformation ON AccountInfo.id = ProfileInformation.uid WHERE UserName = @UName";
            MySqlCommand cmd3 = new MySqlCommand(LoadSavedProfileData, connection);
            cmd3.Parameters.AddWithValue("@UName", runTimeSave.UserName);
            MySqlDataReader rd = cmd3.ExecuteReader();
            while (await rd.ReadAsync())
            {
                log.Info("Loading Initial Data...");
                if (!DBNull.Value.Equals(rd["Description"]))
                {
                    ProfileSave.Description = (string)rd["Description"];
                }
                if (!DBNull.Value.Equals(rd["Name"]))
                {
                    ProfileSave.Name = (string)rd["Name"];
                }
                if (!DBNull.Value.Equals(rd["Fullname"]))
                {
                    ProfileSave.FullName = (string)rd["Fullname"];
                }
                if (!DBNull.Value.Equals(rd["Location"]))
                {
                    ProfileSave.Location = (string)rd["Location"];
                }
                if (!DBNull.Value.Equals(rd["PhoneNumber"]))
                {
                    ProfileSave.PhoneNumber = (string)rd["PhoneNumber"];
                }
                if (!DBNull.Value.Equals(rd["Contact"]))
                {
                    ProfileSave.Contact = (string)rd["Contact"];
                }
                if (!DBNull.Value.Equals(rd["LikeList"]))
                {
                    ListAsString = (string)rd["LikeList"];
                }
                log.Info("Inital Data Loaded!");
            }
            if (ListAsString.Length > 1)
            {
                ProfileSave.UserLikeList = ListAsString.Split(',').ToList();
            }
            ProfileSave.InitialLoad = false;
            await connection.CloseAsync();
            log.Debug("Deactivating Profile Load until App Restarts and Finished Init Load");
            page.LoadingGrid.Visibility = System.Windows.Visibility.Hidden;
            page.AccountFrame.Visibility = System.Windows.Visibility.Visible;
        }

        public async Task ApplyInfoChanges(UserInfoPage page)
        {
            await Task.Delay(1100);
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel Profile Information Update");
                runTimeSave.notifier.ShowError("Es ist ein Verbindungs-Fehler aufgetreten. Bitte versuche es erneut oder klicke hier" +
                    " um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                return;
            }
            string InsertInfoData = "UPDATE ProfileInformation, AccountInfo SET ProfileInformation.Name = @Name, ProfileInformation.Fullname = @FullName, ProfileInformation.Location = @Loc, ProfileInformation.PhoneNumber = @number, ProfileInformation.Contact = @contact WHERE AccountInfo.id = ProfileInformation.uid AND AccountInfo.UserName = @UName";
            MySqlCommand cmd = new MySqlCommand(InsertInfoData, connection);
            cmd.Parameters.AddWithValue("@Name", page.txtBoxName.Text);
            cmd.Parameters.AddWithValue("@FullName", page.txtBoxFullName.Text);
            cmd.Parameters.AddWithValue("@Loc", page.txtBoxLocation.Text);
            cmd.Parameters.AddWithValue("@number", page.txtBoxMobileNumber.Text);
            cmd.Parameters.AddWithValue("@contact", page.txtBoxContact.Text);
            cmd.Parameters.AddWithValue("@UName", runTimeSave.UserName);
            try
            {
                await cmd.ExecuteNonQueryAsync();
                log.Debug("Successfully Insertet Data to UserProfile");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Insert Data to User Profile with Query: " + InsertInfoData + " Exception: ", ex);
                runTimeSave.notifier.ShowError("Es ist ein Datenbank-Fehler aufgetreten.Bitte versuche es erneut oder klicke hier" +
                    " um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                await connection.CloseAsync();
                return;
            }
            ProfileSave.Name = page.txtBoxName.Text;
            ProfileSave.FullName = page.txtBoxFullName.Text;
            ProfileSave.Location = page.txtBoxLocation.Text;
            ProfileSave.PhoneNumber = page.txtBoxMobileNumber.Text;
            ProfileSave.Contact = page.txtBoxContact.Text;
            log.Info("Finished Profile Update");
            await connection.CloseAsync();
            runTimeSave.notifier.ShowSuccess("Profil Informationen erfolgreich Aktualisiert!");
        }

        public async Task ApplyDescriptionChanges(UserAboutPage page)
        {
            await Task.Delay(1100);
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel Profile Description Update");
                runTimeSave.notifier.ShowError("Es ist ein Verbindungs-Fehler aufgetreten. Bitte versuche es erneut oder klicke hier" +
                    " um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                return;
            }
            string UpdateDescription = "UPDATE ProfileInformation, AccountInfo SET ProfileInformation.Description = @Desc WHERE AccountInfo.id = ProfileInformation.uid AND UserName = @UName";
            MySqlCommand cmd = new MySqlCommand(UpdateDescription, connection);
            cmd.Parameters.AddWithValue("@Desc", page.txtBoxAboutMe.Text);
            cmd.Parameters.AddWithValue("@UName", runTimeSave.UserName);
            try
            {
                await cmd.ExecuteNonQueryAsync();
                log.Debug("Successfully Updated Description");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Update Description with query: " + UpdateDescription + " Exception: ", ex);
                runTimeSave.notifier.ShowError("Es ist ein Datenbank-Fehler aufgetreten. Bitte versuche es erneut oder klicke hier " +
                    "um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                await connection.CloseAsync();
                return;
            }
            await connection.CloseAsync();
            log.Info("Description has been Updated");
            runTimeSave.notifier.ShowSuccess("Über mich wurde Aktualisiert!");
            ProfileSave.Description = page.txtBoxAboutMe.Text;
        }

        public async Task ApplyListChanges(UserLikesPage page)
        {
            await Task.Delay(1100);
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel Profile Description Update");
                runTimeSave.notifier.ShowError("Es ist ein Verbindungs-Fehler aufgetreten. Bitte versuche es erneut oder klicke hier" +
                    " um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                return;
            }
            List<string> LikeList = new List<string>();
            foreach (string item in page.listBoxAddedItems.Items)
            {
                LikeList.Add(item);
                ProfileSave.UserLikeList.Add(item);
            }
            string ListAsString = string.Join(",", LikeList);
            if (ListAsString.Length > 2999)
            {
                runTimeSave.notifier.ShowWarning("Die Liste ist zu lang. Bitte entfernen sie ein paar Inhalte!");
                await connection.CloseAsync();
                return;
            }
            string UpdateUserLikeList = "UPDATE ProfileInformation, AccountInfo SET ProfileInformation.LikeList = @list WHERE AccountInfo.id = ProfileInformation.uid AND UserName = @UName";
            MySqlCommand cmd = new MySqlCommand(UpdateUserLikeList, connection);
            cmd.Parameters.AddWithValue("@list", ListAsString);
            cmd.Parameters.AddWithValue("@UName", runTimeSave.UserName);
            try
            {
                await cmd.ExecuteNonQueryAsync();
                log.Debug("LikeList has been Updated");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Update LikeList with query: " + UpdateUserLikeList + " Exception: ", ex);
                runTimeSave.notifier.ShowError("Es ist ein Datenbank-Fehler aufgetreten. Bitte versuche es erneut oder klicke hier " +
                    "um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                await connection.CloseAsync();
                return;
            }
            await connection.CloseAsync();
            runTimeSave.notifier.ShowSuccess("Liste wurde gespeichert!");
        }

        public async Task DeleteUserProfile(AccountManagementPage page)
        {
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel Profile Deletion");
                runTimeSave.notifier.ShowError("Fehler beim Löschen des Profils. Bitte versuchen sie es erneut oder klicken sie hier um sich an den Support zu wenden!", runTimeSave.OpenSupportOption);
                return;
            }
            string DeleteUserProfile = "DELETE ProfileInformation FROM ProfileInformation INNER JOIN AccountInfo ON AccountInfo.id = ProfileInformation.uid WHERE UserName = @UName";
            MySqlCommand cmd = new MySqlCommand(DeleteUserProfile, connection);
            cmd.Parameters.AddWithValue("@UName", runTimeSave.UserName);
            try
            {
                await cmd.ExecuteNonQueryAsync();
                log.Debug("Profile Deleted Successfully!");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Delete User Profile with query: " + DeleteUserProfile + " Exception: ", ex);
                runTimeSave.notifier.ShowError("Fehler beim Löschen des Profils! Bitte versuchen sie es erneut oder klicken sie hier um zum Support zu kommen.", runTimeSave.OpenSupportOption);
                return;
            }
            log.Info("Profile has been Deleted!");
            await connection.CloseAsync();
            ProfileSave.InitialLoad = true;
            runTimeSave.menu.mainFrame.Navigate(new WelcomePage());
            runTimeSave.notifier.ShowSuccess("Profil wurde gelöscht!");
        }

        public async Task LoadAllUsers(ListUserPage page)
        {
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel User Loading");
                runTimeSave.notifier.ShowError("Fehler beim Laden der Nutzer. Bitte versuchen sie es erneut oder klicken sie hier um sich an den Support zu wenden!", runTimeSave.OpenSupportOption);
                return;
            }
            string GetAllUserNames = "SELECT UserName FROM AccountInfo";
            MySqlCommand cmd = new MySqlCommand(GetAllUserNames, connection);
            MySqlDataReader rd = cmd.ExecuteReader();
            while (await rd.ReadAsync())
            {
                log.Debug("Creating UserControl...");
                string UName = (string)rd["UserName"];
                UserControls.UserList control = new UserControls.UserList();
                control.UserNameData = UName;
                control.Margin = new System.Windows.Thickness(0,8,0,0);
                page.stackPanelAllUsers.Children.Add(control);
            }
            await connection.CloseAsync();
            log.Info("Users have been Loaded");
        }

        public async Task LoadUserData(ShowPublicUserData page)
        {
            if (await OpenSqlConnection() == false)
            {
                log.Info("Cancel Data Loading");
                runTimeSave.notifier.ShowError("Fehler beim Laden der Nutzer. Bitte versuchen sie es erneut oder klicken sie hier um sich an den Support zu wenden!", runTimeSave.OpenSupportOption);
                return;
            }
            string CheckIfUserProfileExists = "SELECT uid FROM AccountInfo INNER JOIN ProfileInformation ON AccountInfo.id = ProfileInformation.uid WHERE UserName = @UName";
            MySqlCommand cmd = new MySqlCommand(CheckIfUserProfileExists, connection);
            cmd.Parameters.AddWithValue("@UName", page.Name);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (!await reader.ReadAsync())
            {
                log.Info("User Profile doesnt exists. Leaving Public Profile");
                reader.Close();
                runTimeSave.menu.mainFrame.Navigate(new ListUserPage());
                runTimeSave.notifier.ShowWarning("Nutzer besitzt kein Profil!");
                await connection.CloseAsync();
                return;
            }
            reader.Close();
            string ListAsString = string.Empty;
            string LoadSavedProfileData = "SELECT Description, Name, Fullname, Location, PhoneNumber, Contact, LikeList FROM AccountInfo INNER JOIN ProfileInformation ON AccountInfo.id = ProfileInformation.uid WHERE UserName = @UName";
            MySqlCommand cmd3 = new MySqlCommand(LoadSavedProfileData, connection);
            cmd3.Parameters.AddWithValue("@UName", page.Name);
            MySqlDataReader rd = cmd3.ExecuteReader();
            while (await rd.ReadAsync())
            {
                log.Info("Loading Initial Data...");
                if (!DBNull.Value.Equals(rd["Description"]))
                {
                    PublicSave.Description = (string)rd["Description"];
                }
                if (!DBNull.Value.Equals(rd["Name"]))
                {
                    PublicSave.Name = (string)rd["Name"];
                }
                if (!DBNull.Value.Equals(rd["Fullname"]))
                {
                    PublicSave.FullName = (string)rd["Fullname"];
                }
                if (!DBNull.Value.Equals(rd["Location"]))
                {
                    PublicSave.Location = (string)rd["Location"];
                }
                if (!DBNull.Value.Equals(rd["PhoneNumber"]))
                {
                    PublicSave.PhoneNumber = (string)rd["PhoneNumber"];
                }
                if (!DBNull.Value.Equals(rd["Contact"]))
                {
                    PublicSave.Contact = (string)rd["Contact"];
                }
                if (!DBNull.Value.Equals(rd["LikeList"]))
                {
                    ListAsString = (string)rd["LikeList"];
                }
                log.Info("Inital Data Loaded!");
            }
            if (ListAsString.Length > 1)
            {
                PublicSave.UserLikeList = ListAsString.Split(',').ToList();
            }
            await connection.CloseAsync();
            log.Debug("Deactivating Profile Load until App Restarts and Finished Init Load");
        }
    }
}
