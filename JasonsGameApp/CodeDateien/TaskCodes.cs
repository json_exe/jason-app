﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JasonsGameApp.localStore;
using MySql.Data.MySqlClient;
using ToastNotifications.Messages;

namespace JasonsGameApp.CodeDateien
{
    public class TaskCodes
    {

        private static TaskCodes instance = null;
        private static readonly object padlock = new object();

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        //Some RunTimeInfos for the Task Section
        public bool AppStartedAtNewDay = false;
        public bool UserLogsIn = false;
        //Coins to Collect
        public int CollectableDevCoins = 0;
        private MySqlConnection connection;

        private TaskCodes()
        {

        }

        public static TaskCodes Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new TaskCodes();
                    }
                    return instance;
                }
            }
        }

#pragma warning disable CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
        public async Task CatchStartTime()
#pragma warning restore CS1998 // Bei der asynchronen Methode fehlen "await"-Operatoren. Die Methode wird synchron ausgeführt.
        {
            if (TaskSettings.Default.LoginTaskDate < DateTime.Now.AddHours(-24))
            {
                TaskSettings.Default.LoginTaskDate = DateTime.Now;
                AppStartedAtNewDay = true;
                runTimeSave.notifier.ShowInformation("Du hast eine Task abgeschlossen! Schau im Task Bereich nach.");
            }
            else if (TaskSettings.Default.LoginTaskDate == null)
            {
                TaskSettings.Default.LoginTaskDate = DateTime.Now;
            }
            TaskSettings.Default.Save();
        }

        bool conFail = false;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                runTimeSave.notifier.ShowError("Fehler beim Verbinden zur Datenbank");
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                conFail = true;
            }
        }

        public void AddDevCoins()
        {
            OpenSqlConnection();
            if (conFail == true) return;
            string IncrementDevCoins = "SELECT WalletCount FROM WalletSystem WHERE UserName = @Username";
            MySqlCommand cmd = new MySqlCommand(IncrementDevCoins, connection);
            cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
            int CurDevCoins = 0;
            try
            {
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CurDevCoins = (int)reader["WalletCount"];
                    log.Info($"Set UserCurWalletCount to {CurDevCoins}");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                log.Error("Failed to Retrieve Current DevCoins with Query: " + IncrementDevCoins + " Exception: ", ex);
                runTimeSave.notifier.ShowError("Fehler beim abrufen des Aktuellen Guthabens! Wenden sie sich bitte an den Support");
                return;
            }
            int NewWallet = CurDevCoins + CollectableDevCoins;
            string IncrementDevCoins2 = "UPDATE WalletSystem SET WalletCount = @NewWalletCount WHERE UserName = @Username";
            MySqlCommand cmd2 = new MySqlCommand(IncrementDevCoins2, connection);
            cmd2.Parameters.AddWithValue("@NewWalletCount", NewWallet);
            cmd2.Parameters.AddWithValue("@Username", runTimeSave.UserName);
            try
            {
                cmd2.ExecuteScalar();
                log.Info($"UserWallet Successfully Updated to {CurDevCoins + CollectableDevCoins}");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Increment new DevCoins with Query: " + IncrementDevCoins + " Exception: ", ex);
                runTimeSave.notifier.ShowError("Fehler beim erhöhen des Guthabens! Wenden sie sich bitte an den Support");
                return;
            }
            CollectableDevCoins = 0;
            connection.Close();
        }
    }
}
