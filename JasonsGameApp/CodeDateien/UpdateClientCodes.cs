﻿using JasonsGameApp.Windows;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace JasonsGameApp.CodeDateien
{
    public class UpdateClientCodes
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        UpdateClient updateClient;
        string TempPath = Path.GetTempPath();
        
        //Methode um den Installer für das Update herunterzuladen
        public void WhatToDownload(string DownloadType, UpdateClient updateclient)
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\dist"))
            {
                DirectoryInfo di = Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\dist");
                di.Attributes = FileAttributes.Hidden;
            }
            this.updateClient = updateclient;
            //Checking which Version Should be Downloaded
            if (runTimeSave.IsOnline == false) return;
            if (DownloadType == "Beta-Version")
            {
                try
                {
                    log.Debug("Starting Download for the Beta-Setup");
                    WebClient client = new WebClient();
                    client.DownloadFileAsync(new Uri("https://jsprivatenextcloud.ddns.net/index.php/s/Cr8YtzS3DHScord/download/JasonsAppInstallBeta.exe"), Directory.GetCurrentDirectory() + "\\dist\\Setup.exe");
                    client.DownloadProgressChanged += Client_DownloadProgressChanged;
                    client.DownloadFileCompleted += Client_DownloadFileCompleted;
                }
                catch (Exception ex)
                {
                    log.Error("Failed to Download Beta-Setup. ", ex);
                    MessageBox.Show("Failed to Download Setup. Please Check: \n -You got an Inet Connection \n " +
                        "-You have enough Space on your Drive \n Otherwise please Contact Support and add newest Log" +
                        " File!");
                    return;
                }
            }
            else if (DownloadType == "Alpha-Version")
            {
                MessageBox.Show("Dies hätte nicht passieren sollen...");
            }
            else if (DownloadType == "Stable-Version")
            {
                try
                {
                    WebClient client = new WebClient();
                    client.DownloadFileAsync(new Uri("https://www.dropbox.com/s/nxus7ub9n7pptey/JasonsAppInstall.exe?dl=1"), Directory.GetCurrentDirectory() + "\\dist\\Setup.exe");
                    client.DownloadProgressChanged += Client_DownloadProgressChanged;
                    client.DownloadFileCompleted += Client_DownloadFileCompleted;
                }
                catch (Exception ex)
                {
                    log.Error("Cant Download Stable Setup.", ex);
                    MessageBox.Show("Failed to Download Setup. Please Check: \n -You got an Inet Connection \n " +
                        "-You have enough Space on your Drive \n Otherwise please Contact Support and add newest Log" +
                        " File!");
                    return;
                }                
            }
            else
            {
                MessageBox.Show("Update-Kanal konnte nicht Abgerufen werden. Bitte wenden sie sich an den" +
                    " Support.", "Update Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.Error("Cant retrieve Update Channel. ErrorCode: UC001");
            }
        }

        //Wird ausgeführt wenn der Installer Installiert wurde
        private void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            log.Info("Update Downloaded Successfully");
            MessageBox.Show("Der Download wurde Erfolgreich Abgeschlossen! Drücken sie auf OK um das Endgültige" +
                " Setup zu starten!");
            try
            {
                //Startet das Heruntergeladene Setup im TMP-Ordner
                Process.Start(TempPath + "\\Setup.exe");
                log.Info("Starting Update-Setup");
            }
            catch (Exception ex)
            {
                log.Error("Cannot start Downloaded Setup.", ex);
                MessageBox.Show("Failed to Start Setup. Please Check: \n -That you have all Permissions to Access " +
                    "Temporary Folder \n -Your Anti-Virus isnt Blocking the Setup. \n Otherwise please contact Support" +
                    "and add the newest Log File!");
                return;
            }
            updateClient.Close();
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            updateClient.pbDownloaded.Value = e.ProgressPercentage;
            updateClient.lblShowDownloadInMB.Content = e.BytesReceived + " from " + e.TotalBytesToReceive;
            log.Debug("Download-Progress has Changed");
        }
    }
}
