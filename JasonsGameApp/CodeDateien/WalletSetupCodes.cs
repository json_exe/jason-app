﻿using JasonsGameApp.Pages;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Navigation;
using ToastNotifications.Messages;

namespace JasonsGameApp.CodeDateien
{
    public class WalletSetupCodes
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private MySqlConnection connection;

        public async Task CreateWallet(string WalletPassword, NavigationService service)
        {
            bool Success = await OpenSqlConnection();
            if (!Success)
            {
                log.Error("Wallet Creation Aborted!");
                runTimeSave.notifier.ShowError("Fehler beim erstellen des Wallets");
                return;
            }
            if (WalletPassword == String.Empty || WalletPassword == null || WalletPassword == "")
            {
                string CreateWallet = "INSERT INTO WalletSystem (UserName, IsUsingWallet, WalletCount, IsUsingWalletPassword) VALUES (@Username, 1, 0, 0)";
                MySqlCommand cmd = new MySqlCommand(CreateWallet, connection);
                cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                    runTimeSave.notifier.ShowSuccess("Wallet wurde erfolgreich erstellt!");
                    service.Navigate(new UserWallet());
                }
                catch (Exception ex)
                {
                    log.Error($"Failed to Execute SQL-Query: {CreateWallet}. Exception: ", ex);
                    runTimeSave.notifier.ShowError("Fehler beim erstellen des Wallets!");
                    MessageBox.Show("Es ist ein Fehler aufgetreten. Versuchen sie es erneut oder wenden sie sich an den Support mit der neusten Log Datei!");
                    return;
                }
            }
            else
            {
                string CreateWallet = "INSERT INTO WalletSystem (UserName, IsUsingWallet, WalletCount, IsUsingWalletPassword, WalletPassword) VALUES (@Username, 1, 0, 1, @Password)";
                MySqlCommand cmd = new MySqlCommand(CreateWallet, connection);
                cmd.Parameters.AddWithValue("@Username", runTimeSave.UserName);
                cmd.Parameters.AddWithValue("@Password", await PasswortVerschlüsseln(WalletPassword));
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                    runTimeSave.notifier.ShowSuccess("Wallet wurde erfolgreich erstellt!");
                    Properties.Settings.Default.WalletInUse = true;
                    Properties.Settings.Default.Save();
                    service.Navigate(new UserWallet());
                }
                catch (Exception ex)
                {
                    log.Error($"Failed to Execute SQL-Query: {CreateWallet}. Exception: ", ex);
                    runTimeSave.notifier.ShowError("Fehler beim erstellen des Wallets!");
                    MessageBox.Show("Es ist ein Fehler aufgetreten. Versuchen sie es erneut oder wenden sie sich an den Support mit der neusten Log Datei!");
                    return;
                }
            }
            connection.Close();
        }

        private async Task<bool> OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                await connection.OpenAsync();
                log.Debug("Connection Established");
                return true;
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                return false;
            }
        }

#pragma warning disable CS1998
        private async Task<string> PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Encrypting Password");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Password Encrypted");
                return builder.ToString();
            }
        }

    }
}
