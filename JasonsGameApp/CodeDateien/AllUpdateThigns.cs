﻿using JasonsGameApp.Pages;
using JasonsGameApp.Windows;
using System.Linq;
using System.Windows;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.CodeDateien
{
    public class AllUpdateThigns
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        //Methode zum prüfen welcher Update-Kanal ausgewählt wurde.
        public void WhatIsSelected(Update update)
        {
            if (runTimeSave.IsOnline == false) return;
            //Prüft nun ob Stable, Beta, Alpha oder nichts ausgewählt wurde.
            if (update.DropDownMenu.txtBoxSelection.Text == "Select")
            {
                log.Debug("Nothing Selected");
                MessageBox.Show("Bitte wähle etwas aus dem DropDown Menü!");
            }
            else if (update.DropDownMenu.txtBoxSelection.Text == "Stable-Version")
            {
                log.Info("Stable-Version selected");
                string WhatToDownload = update.DropDownMenu.txtBoxSelection.Text;
                log.Info("Searching for Menu and Closing");
                UpdateClient updateClient = new UpdateClient(WhatToDownload);
                log.Info("Instanciate UpdateClient and Showing");
                //Ruft den update Client auf
                updateClient.Show();
                Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "MainMenu");
                win.Hide();
            }
            else if (update.DropDownMenu.txtBoxSelection.Text == "Alpha-Version" || update.DropDownMenu.txtBoxSelection.Text == "Beta-Version")
            {
                UpdateClient updateClient = new UpdateClient(update.DropDownMenu.txtBoxSelection.Text);
                updateClient.Show();
                log.Info("Instanciated UpdateClient");
                runTimeSave.menu.Hide();
            }
            else
            {
                log.Debug("This was unexpected");
            }
        }


    }
}
