﻿using System.Net;

namespace JasonsGameApp.CodeDateien
{
    public class CheckConnection
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Durchschnittliche Zeit zur Anfrage stellung und Ergebnis erhalt
        //200 bis 350 Millisekunden
        public bool RequestAsync()
        {
            log.Debug("Checking Connection");
            string url = "https://jsprivatenextcloud.ddns.net/";
            //Versucht eine Response von der angegebenen Webseite zu erhalten und gibt je nachdem einen Bool zurück
            HttpWebRequest httpWebResponse = (HttpWebRequest)WebRequest.Create(url);
            httpWebResponse.AllowAutoRedirect = false;
            httpWebResponse.Timeout = 5000;
            //Ein Integer Array mit allen wichtigen HTTP Fehlecodes
            int[] StatusCodes = new int[] { 400, 401, 403, 408, 413, 421, 425, 429, 451, 500, 502, 503, 504};

            try
            {
                //Macht eine Anfrage an eine URL und gibt dessen StatusCode zurück
                HttpWebResponse response = (HttpWebResponse)httpWebResponse.GetResponse();
                //Testen des erhaltenen Statuscodes auf die Fehlerstatus Codes im Array
                foreach (var item in StatusCodes)
                {
                    if ((int)response.StatusCode == item)
                    {
                        //Gebe false zurück falls einer der Fehler Codes erhalten wurde
                        log.Error("Connection to App Server Failed. StatusCode: " + (int)response.StatusCode);
                        return false;
                    }
                }
                //Gebe true zurück wenn keine Fehlercodes vorhanden sind
                log.Info("Connection to Server Established");
                return true;
            }
            catch (System.Exception ex)
            {
                //Gebe false zurück falls es in der HttpWebResponse Anfrage Fehler gibt
                log.Error("Cant connect to App-Server. Failed to recieve Response.", ex);
                return false;
            }
        }
    }
}
