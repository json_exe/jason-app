﻿using JasonsGameApp.Windows;
using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ToastNotifications;
using ToastNotifications.Core;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace JasonsGameApp.CodeDateien
{
    public sealed class RunTimeSingletonSave
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static RunTimeSingletonSave instance = null;
        private static readonly object padlock = new object();
        //Bool zum Checken ob man eingeloggt ist
        public bool IsLoggedIn = false;
        //String in dem der Aktuelle Benutzer gespeichert wird
        public string UserName;
        //bool zum Checken ob die News bereits geladen wurden
        public bool RetrievedNews = false;
        //Bool zum Prüfen ob man Online ist oder nicht.
        public bool IsOnline;
        //Menü window
        public NewMenu menu;
        //Creating a WebClient
        public WebClient ServerClient = new WebClient();
        //bool zum Überprüfen ob der Wallet entsperrt ist
        public bool WalletUnlocked = false;
        //int für die UID
        public int UID;
        //
        private DispatcherTimer timer = new DispatcherTimer();

        private RunTimeSingletonSave()
        {

        }

        public static RunTimeSingletonSave Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new RunTimeSingletonSave();
                    }
                    return instance;
                }
            }
        }

        public Notifier notifier = new Notifier(cfg =>
        {
            cfg.DisplayOptions.TopMost = true;
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.TopRight,
                offsetX: 30,
                offsetY: 50);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(5),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        public MessageOptions OpenSupportOption = new MessageOptions
        {
            FontSize = 30,
            ShowCloseButton = true,
            FreezeOnMouseEnter = true,
            NotificationClickAction = n =>
            {
                n.Close();
                
            }
        };

        private MySqlConnection connection;

        public async void RetrieveUserID()
        {
            Stream stream = ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                await connection.OpenAsync();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                notifier.ShowError("Verbindungs-Fehler. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab -das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                IsOnline = false;
                return;
            }
            string GetUID = "SELECT id FROM AccountInfo WHERE UserName = @Username";
            MySqlCommand cmd = new MySqlCommand(GetUID, connection);
            cmd.Parameters.AddWithValue("@Username", UserName);
            MySqlDataReader rd = (MySqlDataReader)await cmd.ExecuteReaderAsync();
            while (rd.Read())
            {
                UID = (int)rd["id"];
            }
        }

        public bool RetrieveMaintenaceState()
        {
            timer.Interval = new TimeSpan(0, 0, 60);
            timer.Tick += Timer_Tick;
            Stream stream = ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                connection.Close();
                return true;
            }
            string GetUID = "SELECT MaintenanceMode FROM AppSpecificInfo WHERE AppSpecificInfo.MaintenanceMode = True";
            MySqlCommand cmd = new MySqlCommand(GetUID, connection);
            MySqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                connection.Close();
                return true;
            }
            else
            {
                timer.Start();
                connection.Close();
                return false;
            }
        }

        private async Task<bool> RetrieveMaintenaceStateAsync()
        {
            Stream stream = ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(await reader.ReadToEndAsync());
            try
            {
                await connection.OpenAsync();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                notifier.ShowError("Es konnte keine Verbindung zur Datenbank hergestellt werden. Service wird" +
                    " in den Wartungsmodus versetzt. Ursachen: " +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, as der Server momentan durch Wartung Offline ist \n -Bei Fragen und weiterem wenden " +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                await connection.CloseAsync();
                //return true;
            }
            string GetUID = "SELECT MaintenanceMode FROM AppSpecificInfo WHERE AppSpecificInfo.MaintenanceMode = True";
            MySqlCommand cmd = new MySqlCommand(GetUID, connection);
            MySqlDataReader rd = cmd.ExecuteReader();
            if (await rd.ReadAsync())
            {
                await connection.CloseAsync();
                notifier.ShowWarning("Service Maintenance Mode has been Activated. Application will Turn to Maintenance Mode in 15" +
                    " Seconds, to prevent any Problems.");
                await Task.Delay(15000);
                return true;
            }
            else
            {
                await connection.CloseAsync();
                return false;
            }
        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            notifier.ShowInformation("Checking Server State... Aware for some Lags...");
            bool test = await RetrieveMaintenaceStateAsync();
            if (test)
            {
                MaintenanceMode mode = new MaintenanceMode();
                mode.Show();
                menu.Close();
                timer.Stop();
                return;
            }
            else
            {
                timer.Interval = new TimeSpan(0, 0, 60);
                timer.Start();
            }
        }
    }
}
