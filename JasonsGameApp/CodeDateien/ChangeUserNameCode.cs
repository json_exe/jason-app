﻿using JasonsGameApp.UserControls;
using MySql.Data.MySqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.CodeDateien
{
    public class ChangeUserNameCode
    {

        MySqlConnection connection;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private bool failed = false;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Info("Opening MySQL Connection");
            }
            catch (System.Exception ex)
            {
                failed = true;
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
            }
        }

        private void CloseSqlConnection()
        {
            connection.Close();
            log.Debug("Closing MySQL Connection");
        }

        //Methode um den Aktuellen Benutzernamen zu ändern
        public void CheckAndChangeUserName(string OldUserName, string NewUserName, string Password, ChangeUsername changeUsername)
        {
            //Verlässt die Methode wenn es Fehler bei der Verbindung gab
            if (runTimeSave.IsOnline == false) return;
            //Öffnet die Datenbank Verbindung
            OpenSqlConnection();
            //Beendet den prozess wenn die Verbindung Fehlschlägt
            if (failed == true) return;
            //Erstellt eine Datenbank Abfrage um zu gucken WER den Nutzernamen ändern möchte und ob das Passwort mit diesem Account übereinstimmt.
            string query = "SELECT * FROM AccountInfo WHERE UserName = @UserName AND Password = @Password";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@UserName", OldUserName);
            cmd.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(Password));
            MySqlDataReader dataReader = cmd.ExecuteReader();
            log.Info("Creating dataReader and Executing");

            //Ließt die Daten aus der Datenbank
            if (dataReader.Read())
            {
                dataReader.Close();
                log.Debug("Closing Data Reader");
                //Setzt für den Aktuellen Nutzer den neuen nutzernamen
                string query2 = "UPDATE AccountInfo SET UserName = @UserName WHERE UserName = @UserName2";
                MySqlCommand cmd2 = new MySqlCommand(query2, connection);
                cmd2.Parameters.AddWithValue("@UserName", NewUserName);
                cmd2.Parameters.AddWithValue("@UserName2", OldUserName);
                try
                {
                    cmd2.ExecuteNonQuery();
                    log.Info("Executing SQL Command NonQuery");
                }
                catch (System.Exception ex)
                {
                    failed = true;
                    log.Error("Cant Execute SQL-Statement: ", ex);
                    MessageBox.Show("ERROR: Failed to Execute SQL Command. Please Contact Support with the newest Log File");
                }
                if (failed == true) return;
                MessageBox.Show("Benutzername wurde Erfolgreich geändert!");
                //Nutzername wird über RunTime gespeichert
                runTimeSave.UserName = NewUserName;
                //Nutzername wird in den Settings gespeichert
                Properties.Settings.Default.UserName = NewUserName;
                Properties.Settings.Default.Save();
                //Textbox vom Menü wird Aktualisiert
                changeUsername.menu.txtBoxLoggedInUser.Text = "Logged In as" + NewUserName;
                //Schließt aktuelle Datenbank Verbindung
                CloseSqlConnection();
                log.Debug("Closing SQL Connection");
                changeUsername.Close();
                log.Debug("Closing Username Window");
            }
            else
            {
                MessageBox.Show("Passwort ist Falsch, bitte Versuche es erneut!", "Password Wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.Debug("User entered Wrong Password");
            }
        }

        //Methode zum Verschlüsseln des Passworts
        private string PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Converting Password to SHA256");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Password Encrypted");
                return builder.ToString();
            }
        }
    }

}
