﻿using JasonsGameApp.Windows;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;
using System.Windows;

namespace JasonsGameApp.CodeDateien
{
    public class SubscribeUnsubscribeMail
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private bool Failed = false;
        int MailInformation;
        MySqlConnection connection;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (System.Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
            }
        }

        public void Check()
        {
            string CurrentUser = runTimeSave.UserName;
            //Öffnen einer Datenbank Verbindung wenn man Online ist
            if (runTimeSave.IsOnline == false) return;
            OpenSqlConnection();
            //Führt den Code weiter wenn die Verbindung erfolgreich war.
            if (Failed == true) return;
            string queryGetEmailStatus = "SELECT RecieveMail FROM AccountInfo WHERE UserName = @UserName";
            MySqlCommand cmd = new MySqlCommand(queryGetEmailStatus, connection);
            cmd.Parameters.AddWithValue("@UserName", CurrentUser);
            MySqlDataReader reader = cmd.ExecuteReader();
            log.Info("Executing MySQL Data Reader");
            //Guckt ob der Nutzer existiert
            while (reader.Read())
            {
                MailInformation = (int)reader["RecieveMail"];
            }
            if (MailInformation == 1)
            {
                //Meldet den Nutzer von E-Mails ab
                log.Debug("User get unsubscribed");
                string queryUnsubscribe = "UPDATE AccountInfo SET RecieveMail = 0 WHERE UserName = @UserName";
                reader.Close();
                log.Debug("Closing Reader");
                MySqlCommand cmd2 = new MySqlCommand(queryUnsubscribe, connection);
                cmd2.Parameters.AddWithValue("@UserName", CurrentUser);
                try
                {
                    cmd2.ExecuteNonQuery();
                    log.Info("Executing MySQL-Command");
                }
                catch (System.Exception ex)
                {
                    log.Error("Cant Execute MySQL-Command to unsubscribe", ex);
                    MessageBox.Show("Failed to Unsubscribe from Mails. Please report this to Support and add the newest" +
                        " Log file");
                    return;
                }
                connection.Close();
                log.Debug("Closing SQL Connection");
                MessageBox.Show("Du wurdest von den Update und Informations E-Mails abgemeldet.");
            }
            else
            {
                //Meldet den Nutzer zu E-Mails an
                log.Debug("Adding User to Subscription List");
                string querySubscribe = "UPDATE AccountInfo SET RecieveMail = 1 WHERE UserName = @UserName";
                reader.Close();
                log.Debug("Closing Reader");
                MySqlCommand cmd3 = new MySqlCommand(querySubscribe, connection);
                cmd3.Parameters.AddWithValue("@UserName", CurrentUser);
                try
                {
                    cmd3.ExecuteNonQuery();
                    log.Info("Subscribed User to Mails");
                }
                catch (System.Exception ex)
                {
                    log.Error("Failed to Execute MySQL Command: Subscribe to Mails", ex);
                    MessageBox.Show("Failed to Subscribe to Mails. Please Report to Support and add the newest Log Files");
                    return;
                }
                connection.Close();
                log.Debug("Closing connection");
                MessageBox.Show("Du wurdest für Update und Informations E-Mails angemeldet! Danke das du Up-to-Date bleiben möchtest!");
            }
            //Schließt die Datenbank Verbindung
            connection.Close();
        }
    }
}
