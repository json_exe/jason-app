﻿using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Threading.Tasks;
using ToastNotifications.Messages;

namespace JasonsGameApp.CodeDateien.MaintenaceCodes
{
    public class MaintenanceCodes
    {
        private MySqlConnection connection;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;


        public async Task<bool> TestState()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(await reader.ReadToEndAsync());
            try
            {
                await connection.OpenAsync();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                runTimeSave.notifier.ShowError("Es konnte keine Verbindung zur Datenbank hergestellt werden. Status konnte nicht überprüft" +
                    " werden!");
                await connection.CloseAsync();
                return true;
            }
            string GetUID = "SELECT MaintenanceMode FROM AppSpecificInfo WHERE AppSpecificInfo.MaintenanceMode = True";
            MySqlCommand cmd = new MySqlCommand(GetUID, connection);
            MySqlDataReader rd = (MySqlDataReader)await cmd.ExecuteReaderAsync();
            if (await rd.ReadAsync())
            {
                await connection.CloseAsync();
                return true;
            }
            else
            {
                await connection.CloseAsync();
                return false;
            }
        }
    }
}
