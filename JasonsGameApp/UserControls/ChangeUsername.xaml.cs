﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.Windows;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.UserControls
{
    /// <summary>
    /// Interaktionslogik für ChangeUsername.xaml
    /// </summary>
    public partial class ChangeUsername : Window
    {
        public NewMenu menu;
        string OldUserName;

        public ChangeUsername(string OldUserName, NewMenu menu)
        {
            InitializeComponent();
            this.menu = menu;
            this.OldUserName = OldUserName;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex("^[a-zA-Z0-9-_.#<>]{3,25}$");
            bool TrueUserName = regex.IsMatch(EnterNewUserName.Text);
            if (EnterNewUserName.Text != string.Empty && EnterNewUserName.Text != "Neuer Benutzername" && TrueUserName == true)
            {
                if (EnterPassword.Password != string.Empty)
                {
                    DialogResult result = MessageBox.Show("Hierdurch wird dein Benutzername von " + OldUserName + " zu " + EnterNewUserName.Text + " geändert!" +
                        " Möchtest du Fortfahren?", "Change UserName?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        string NewUserName = EnterNewUserName.Text;
                        string Password = EnterPassword.Password;
                        ChangeUserNameCode codes = new ChangeUserNameCode();
                        codes.CheckAndChangeUserName(OldUserName, NewUserName, Password, this);
                    }
                    else
                    {
                        MessageBox.Show("Der Benutzername wird nicht geändert!", "Change UserName Aborted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Bitte gebe dein Passwort ein!");
                }
            }
            else
            {
                MessageBox.Show("Bitte gebe einen neuen Benutzernamen in das Eingabefeld ein, der unteranderem unseren Kriterien entspricht!");
            }
        }
    }
}
