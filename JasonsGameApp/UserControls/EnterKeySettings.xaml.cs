﻿using JasonsGameApp.CodeDateien;
using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace JasonsGameApp.UserControls
{
    /// <summary>
    /// Interaktionslogik für EnterKeySettings.xaml
    /// </summary>
    public partial class EnterKeySettings : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        MySqlConnection connection;
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        bool failed = false;

        public EnterKeySettings()
        {
            InitializeComponent();
            log.Info("Instanciated EnterKeySettings Window");
        }

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                failed = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Bitte gebe deinen Key ein!");
        }

        private void btnAbort_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Closing EnterKeySettings Window");
            this.Close();
        }

        private void btnProceed_Click(object sender, RoutedEventArgs e)
        {
            if (failed == true) return;
            log.Info("Starting Key Check");
            //Guckt ob etwas in die TextBoxxen eingegeben ist
            if (txtBoxEnterKey.Text != null)
            {
                //Guckt ob Key in der Textbox Steht
                if (txtBoxEnterKey.Text != "Key")
                {
                    //Erstellen eines Regex zum Prüfen des Key Schemas
                    Regex regex = new Regex("[JDS0-9]{5}-[0-9]{5}-[A-Z]{5}-[0-9]{5}");
                    bool IsValidKey = regex.IsMatch(txtBoxEnterKey.Text);
                    //Prüft das Key Schema
                    if (IsValidKey == true)
                    {
                        OpenSqlConnection();
                        log.Debug("Key Passed Schema Check");
                        string CurrentPath = Directory.GetCurrentDirectory();
                        //Datenbankcommand zum Überprüfen des Schlüssels.
                        string SelectKey = "SELECT SavedKey FROM AccountInfo WHERE SavedKey = @Key";
                        MySqlCommand cmd = new MySqlCommand(SelectKey, connection);
                        cmd.Parameters.AddWithValue("@Key", txtBoxEnterKey.Text);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        log.Info("Execting SQL-Reader with Query SelectKey");
                        if (reader.Read())
                        {
                            MessageBox.Show("Dieser Key ist bereits in Verwendung! Bitte gebe einen anderen Key ein. Sollte dies ein Fehler sein, wende dich bitte an den Support!");
                        }
                        else
                        {
                            reader.Close();
                            string SelectAllKeys = "SELECT * FROM AppKeys WHERE AppKey = @Keys";
                            MySqlCommand cmd2 = new MySqlCommand(SelectAllKeys, connection);
                            cmd2.Parameters.AddWithValue("@Keys", txtBoxEnterKey.Text);
                            MySqlDataReader reader1 = cmd2.ExecuteReader();
                            if (reader1.Read())
                            {
                                reader1.Close();
                                string InsertKeyToUser = "UPDATE AccountInfo SET SavedKey = @TheKey WHERE UserName = @Username";
                                MySqlCommand cmd3 = new MySqlCommand(InsertKeyToUser, connection);
                                cmd3.Parameters.AddWithValue("@TheKey", txtBoxEnterKey.Text);
                                cmd3.Parameters.AddWithValue("@Username", runTimeSave.UserName);
                                try
                                {
                                    cmd3.ExecuteNonQuery();
                                    log.Debug("Successfully Exected SqlQuery InsertKeyToUser");
                                }
                                catch (Exception ex)
                                {
                                    log.Error("Failed to Execute SQL Query InsertKeyToUser", ex);
                                    MessageBox.Show("Fehler beim Speichern des Keys im Nutzeraccount! Bitte überprüfen sie: \n -Das sie eine Internetverbindung haben \n -Das es keine " +
                                        "Ankündigung auf dem Offizielen Discord gab bezüglich Server Fehler \n -Starten sie die App neu \n Sollte all dies nicht helfen" +
                                        " wenden sie sich bittet mit der neusten Log Datei an den Support.");
                                    return;
                                }
                                //Speichert den key Sofern er richtig ist
                                Properties.Settings.Default.ProgramKey = txtBoxEnterKey.Text;
                                Properties.Settings.Default.Save();
                                MessageBox.Show("Key wurde gespeichert und mit deinem Account verknüpft! Wenn sie einen neuen key hinzufügen, wird der Alter überschrieben und ungültig.");
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Der Key ist Falsch oder Existiert nicht! Bitte überprüfe deine Eingaben erneut oder wende dich an den Support!");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Bitte gebe einen richtigen Key ein!");
                    }
                }
                else
                {
                    MessageBox.Show("Bitte gebe einen Key ein!");
                }
            }
            else
            {
                MessageBox.Show("Bitte geben sie etwas in das Eingabefeld ein!");
            }
        }
    }
}
