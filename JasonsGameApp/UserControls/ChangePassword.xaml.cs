﻿using JasonsGameApp.CodeDateien;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JasonsGameApp.UserControls
{
    /// <summary>
    /// Interaktionslogik für ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        MySqlConnection connection;
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        public ChangePassword()
        {
            InitializeComponent();
            OpenSqlConnection();
        }

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                this.Close();
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            //First Checking UserInput
            //Guckt ob einer der Textboxxen Leer ist
            if (txtNewPassword.Password != String.Empty || txtOldPassword.Password != String.Empty || txtReviseNewPassword.Password != String.Empty)
            {
                //Guckt ob das neue Passwort mit der Wiederholung übereinstimmt
                if (txtNewPassword.Password != txtReviseNewPassword.Password)
                {
                    MessageBox.Show("Deine neuen Passwörter stimmen nicht überein! Bitte versuche es erneut.");
                    log.Debug("New Password does not match Password Revise!");
                    return;
                }
                //Guckt zuerst mit dem alten Passwort und den Nutzernamen ob der Nutzer existiert und das Richtige Passwort eingegeben hat
                string sqlQuery = "SELECT Password FROM AccountInfo WHERE UserName = @Username AND Password = @Password";
                MySqlCommand cmd = new MySqlCommand(sqlQuery, connection);
                cmd.Parameters.AddWithValue("@UserName", runTimeSave.UserName);
                cmd.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(txtOldPassword.Password));
                MySqlDataReader rd = cmd.ExecuteReader();
                log.Info("Executing Reader with query: " + sqlQuery);
                if (rd.Read())
                {
                    log.Debug("Password wrong");
                    Regex regexPswd = new Regex("^[a-zA-Z0-9-_!#<>+]{8,50}$");
                    //Prüft ob das Passwort mindestens 8 Zeichen lang ist und nur die erlaubten Sonderzeichen enthält
                    bool testingPSWD1 = regexPswd.IsMatch(txtNewPassword.Password);
                    if (testingPSWD1 == false)
                    {
                        MessageBox.Show("Das neue Passwort entspricht nicht unseren Richtlinien! Bitte beachte das dein Passwort diese Richtlinien einhält: \n " +
                            "-Mindestens 8 Zeichen bis maximal 50 Zeichen \n -Darf diese Sonderzeichen enthalten: - _ ! # < > +");
                        log.Debug("Password does not match Criterias!");
                    }
                    else
                    {
                        rd.Close();
                        //Setzt das neue Passwort für den Angemeldeten nutzer.
                        string query2 = "UPDATE AccountInfo SET Password (@Password) WHERE UserName = @UserName";
                        MySqlCommand cmd2 = new MySqlCommand(query2, connection);
                        cmd2.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(txtNewPassword.Password));
                        cmd2.Parameters.AddWithValue("@UserName", runTimeSave.UserName);
                        try
                        {
                            cmd2.ExecuteNonQuery();
                            log.Debug("Successfully Updated Password");
                            //Setzt die Automatische Anmeldung zurück
                            Properties.Settings.Default.UserName =  String.Empty;
                            Properties.Settings.Default.AlwaysLoggedIn = false;
                            Properties.Settings.Default.Save();
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to Update User Password with query: " + query2 + "Exception: ", ex);
                            MessageBox.Show("Es ist ein Fehler aufgetreten. Das Password konnte nicht geändert werden. Bitte probieren sie es erneut. Sollte der Fehler weiterhin bestehen" +
                                " melden sie sich bitte mit der neusten Log Datei beim Support.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Password ist falsch! Bitte überprüfe dein Passwort!");
                    log.Debug("Password is Wrong");
                }
            }
            else
            {
                MessageBox.Show("Bitte stelle sicher, das du in allen Text Feldern was eingegeben hast!");
                log.Debug("At least one Text Field is Empty!");
            }
        }

        private string PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Encrypting Password");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Password Encrypted");
                return builder.ToString();
            }
        }
    }
}
