﻿using System.Windows;

namespace JasonsGameApp.UserControls
{
    /// <summary>
    /// Interaktionslogik für Select_Update_Channel.xaml
    /// </summary>
    public partial class Select_Update_Channel : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Select_Update_Channel()
        {
            InitializeComponent();
            log.Info("Instanciated SelectUpdateChannel Window");
        }

        private void btnStable_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Stable Version Selected");
            //Speichert den Standard Kanal in den Settings
            Properties.Settings.Default.StandardUpdateChannel = "Stable-Version";
            Properties.Settings.Default.Save();
            MessageBox.Show("Standard Update Kanal wurde auf Stable festgelegt!");
            this.Close();
        }

        private void btnBeta_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Beta Version Selected");
            //Speichert den Standard Kanal in den Settings
            Properties.Settings.Default.StandardUpdateChannel = "Beta-Version";
            Properties.Settings.Default.Save();
            MessageBox.Show("Standard Update Kanal wurde auf Beta festgelegt!");
            this.Close();
        }

        private void btnAlpha_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Alpha Version Selected");
            //Speichert den Standard Kanal in den Settings
            Properties.Settings.Default.StandardUpdateChannel = "Alpha-Version";
            Properties.Settings.Default.Save();
            MessageBox.Show("Standard Update Kanal wurde auf Alpha festgelegt!");
            this.Close();
        }
    }
}
