﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.CodeDateien.UserProfileCodes;
using JasonsGameApp.Pages.AccountPages;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.UserControls
{
    /// <summary>
    /// Interaktionslogik für UserList.xaml
    /// </summary>
    public partial class UserList : UserControl
    {

        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        public UserList()
        {
            InitializeComponent();
        }

        public string UserNameData
        {
            get { return (string)GetValue(UserNameDataProperty); }
            set { SetValue(UserNameDataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserNameData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserNameDataProperty =
            DependencyProperty.Register("UserNameData", typeof(string), typeof(UserList), new PropertyMetadata(null));

        private void lblShowProfile_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            string Name = lblUserName.Content.ToString();
            runTimeSave.menu.mainFrame.Navigate(new ShowPublicUserData(Name));
        }
    }
}
