﻿using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        SignUpCodes code;

        public SignUp()
        {
            InitializeComponent();
            log.Info("Instanciated SignUp Window");
            SignUpCodes code = new SignUpCodes();
            this.code = code;
        }

        private void btnSignUp_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Running User SignUp");
            code.RegisterNewUser(this);
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Opening LoginIn Window");
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void btnShowPswd_Click(object sender, RoutedEventArgs e)
        {
            if (txtShowPassword.Visibility == Visibility.Hidden)
            {
                //C:\Users\lhi\Desktop\jason-app\JasonsGameApp\bin\Debug\Images\icons8-ausblenden-16.png" 
                imgEye.Source = new BitmapImage(new System.Uri(Directory.GetCurrentDirectory() + @"\Images\icons8-ausblenden-16.png"));
                txtShowPassword.Visibility = Visibility.Visible;
                txtShowPassword.Text = EnterPSWD.Password;
                EnterPSWD.Visibility = Visibility.Hidden;
            }
            else
            {
                imgEye.Source = new BitmapImage(new System.Uri(Directory.GetCurrentDirectory() + @"\Images\icons8-sichtbar-16.png"));
                EnterPSWD.Password = txtShowPassword.Text;
                txtShowPassword.Visibility = Visibility.Hidden;
                EnterPSWD.Visibility = Visibility.Visible;
            }
        }
    }
}
