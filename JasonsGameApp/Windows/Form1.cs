﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using JasonsGameApp.Windows;

namespace JasonsGameApp
{
    public partial class splashform : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
            (
                int nLeftRect,
                int nTopRecht,
                int RightRect,
                int nBottomRect,
                int nWidthEllipse,
                int nHeightEllipse
            );
        private NewMenu menu;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public splashform(NewMenu menu)
        {
            InitializeComponent();
            log.Info("Showing SplashScreen");
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));
            ProgressBar1.Value = 0;
            this.menu = menu;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Versteckt das Hauptfenster
            menu.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Guckt sich den Wert des Timers an und zählt den Fortschritt des Fortschrittsbalken immer um 5% hoch
            ProgressBar1.Value += 5;
            ProgressBar1.Text = ProgressBar1.Value.ToString() + "%";

            if (ProgressBar1.Value == 100)
            {
                timer1.Enabled = false;
                menu.Show();
                this.Close();
            }
        }
    }
}
