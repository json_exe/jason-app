﻿using JasonsGameApp.CodeDateien.MaintenaceCodes;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für MaintenanceMode.xaml
    /// </summary>
    public partial class MaintenanceMode : Window
    {
        MaintenanceCodes codes;
        DispatcherTimer timer;
        int Time = 60;

        public MaintenanceMode()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MaintenanceCodes codes = new MaintenanceCodes();
            this.codes = codes;
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            if (Time > 0)
            {
                Time--;
                lblRemainingTime.Content = string.Format("{0}", Time % 60);
            }
            else
            {
                lblRemainingTime.Content = "Checking...";
                await Task.Delay(500);
                bool State = await codes.TestState();
                await Task.Delay(500);
                if (!State)
                {
                    timer.Stop();
                    new NewMenu().Show();
                    this.Close();
                }
                else
                {
                    timer.Stop();
                    timer.Start();
                    Time = 60;
                }
            }
        }
    }
}
