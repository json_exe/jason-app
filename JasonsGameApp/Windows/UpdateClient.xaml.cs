﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.Pages;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using WpfAnimatedGif;
using MessageBox = System.Windows.Forms.MessageBox;

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für UpdateClient.xaml
    /// </summary>
    public partial class UpdateClient : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        int time = 10;
        DispatcherTimer Timer;
        string WhatToDownload;

        public UpdateClient(string WhatDownload)
        {
            InitializeComponent();
            log.Info("Instanciated UpdateClient Window");
            this.WhatToDownload = WhatDownload;
        }

        public UpdateClient(Update update)
        {
            InitializeComponent();
            MessageBox.Show("Hi");
            log.Info("Instanciated UpdateClient Window");
            WhatToDownload = update.DropDownMenu.txtBoxSelection.Text;
        }

        private void UpdateCounter_Click(object sender, RoutedEventArgs e)
        {
            ///Nun kann der User bevor das Update startet, diesen nun Abbrechen.
            ///Nach dem Abbrechen wird nochmal gefragt ob man das Update vielleicht
            ///nicht wiederaufnehmen möchte, falls es ein Versehen war.
            Timer.Stop();
            log.Debug("Timer Aborted");
            DialogResult result = (DialogResult)System.Windows.MessageBox.Show("Update wurde Abgebrochen. Möchten sie es wiederaufnehmen?"
                , "Update Aborted", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                log.Debug("Resuming Timer");
                System.Windows.MessageBox.Show("Mit dem druck auf OK wird das Update fortgesetzt!",
                    "Resume Update");
                Timer.Start();
            }
            else
            {
                runTimeSave.menu.Show();
                log.Debug("Closing UpdateClient");
                this.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Startet den Download timer wenn das Fenster geladen wird
            Timer = new DispatcherTimer();
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Tick += Timer_Tick;
            Timer.Start();
            log.Debug("Started Timer");
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //Rechnet jeden Tick des Timers die Zeit time -
            if (time > 0)
            {
                time--;
                UpdateCounter.Content = string.Format("Starting in {0}", time % 60);
            }
            else
            {
                //Stoppt den Timer, versteckt den Button, lädt ein Gif und startet den Download
                Timer.Stop();
                runTimeSave.menu.Close();
                UpdateCounter.Visibility = Visibility.Hidden;
                pbDownloaded.Visibility = Visibility.Visible;
                string GifDirectory = Directory.GetCurrentDirectory();
                var image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(GifDirectory + "\\Gifs\\update.gif");
                image.EndInit();
                log.Debug("Showing Gif");
                ImageBehavior.SetAnimatedSource(UpdateGif, image);
                lblAbort.Visibility = Visibility.Hidden;
                UpdateClientCodes codes = new UpdateClientCodes();
                log.Info("Running UpdateCodes");
                codes.WhatToDownload(WhatToDownload, this);
                //Auswerten welches Update Installiert werden soll (Beta/Alpha oder Stable)
                //und dann das Update Installieren. (Herunterladen geschieht nun voll
                //Automatisch ohne das der User dazwischen Funken muss!)
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            
        }
    }
}
