﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using WpfAnimatedGif;

namespace JasonsGameApp
{
    /// <summary>
    /// Interaktionslogik für DownloadWizard.xaml
    /// </summary>
    public partial class DownloadWizard : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string NameOfDownload;
        DownloadCenter download;

        public DownloadWizard(DownloadCenter downloadCenter)
        {
            this.download = downloadCenter;
            log.Info("Instanciated DownloadWizard");
            InitializeComponent();
        }

        //Wird aufgerufen wenn das Fenster lädt
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            NameOfDownload = download.WhichDownload;
            MessageBox.Show("Um den Download zu starten, drücken sie auf den Knopf. Der Download wird in diesem Fenster im Hintergrund" +
                " ausgeführt. Nach Abschluss des Download (sofern möglich) wird die Anwendung gestartet.", "Tutorial", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            //Wird ausgeführt wenn man auf den Download Button klickt.
            //Setzt ein Gif und lässt den Download Button Verschwinden. Ebenso wird der Fortschrittsbalken angezeigt
            log.Debug("Download Button Clicked");
            string Path = Directory.GetCurrentDirectory();
            btnDownload.Visibility = Visibility.Hidden;
            DownloadGif.Visibility = Visibility.Visible;
            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri(Path + "\\Gifs\\download.gif");
            image.EndInit();
            ImageBehavior.SetAnimatedSource(DownloadGif, image);
            MainCodes.Codes codes = new MainCodes.Codes();
            log.Debug("Calling MainCodes File");
            codes.WhatToDownload(this, NameOfDownload);
        }

        //Wird ausgeführt wenn sich das Fenster schließt
        private void WizardWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }
    }
}
