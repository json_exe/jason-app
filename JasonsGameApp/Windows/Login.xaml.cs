﻿using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Login()
        {
            InitializeComponent();
            log.Info("Instanciated Login Window");
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Performing Login");
            LoginCode codes = new LoginCode();
            codes.PerformLogin(this);
        }

        private void btnSignUp_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Opening SignUp Window");
            SignUp sign = new SignUp();
            sign.Show();
            this.Close();
        }

        private void checkBoxAlwaysLoggedIn_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnShowPswd_Click(object sender, RoutedEventArgs e)
        {
            if (txtShowPassword.Visibility == Visibility.Hidden)
            {
                //C:\Users\lhi\Desktop\jason-app\JasonsGameApp\bin\Debug\Images\icons8-ausblenden-16.png" 
                imgEye.Source = new BitmapImage(new System.Uri(Directory.GetCurrentDirectory() + @"\Images\icons8-ausblenden-16.png"));
                txtShowPassword.Visibility = Visibility.Visible;
                txtShowPassword.Text = EnterPSWD.Password;
                EnterPSWD.Visibility = Visibility.Hidden;
            }
            else
            {
                imgEye.Source = new BitmapImage(new System.Uri(Directory.GetCurrentDirectory() + @"\Images\icons8-sichtbar-16.png"));
                EnterPSWD.Password = txtShowPassword.Text;
                txtShowPassword.Visibility = Visibility.Hidden;
                EnterPSWD.Visibility = Visibility.Visible;
            }
        }
    }
}
