﻿using JasonsGameApp.CodeDateien;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für _3PointMenu.xaml
    /// </summary>
    public partial class _3PointMenu : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;

        public _3PointMenu()
        {
            InitializeComponent();
            log.Info("Instanciated 3PointMenu");
        }

        private void btnResetPassword_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) return;
            log.Debug("Reset Password Clicked");
            //Öffnet das ResetPassword Fenster
            ResetPassword resetPassword = new ResetPassword();
            resetPassword.Show();
            //Schließt das SignUp und Login Fenster sofern offen
            Window win3 = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "SignUpWindow");
            Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "LoginWindow");
            if (win3 != null)
            {
                win3.Close();
            }
            else if (win != null)
            {
                win.Close();
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Exiting");
            //Schließt das Login/SignUp Fenster sofern offen und öffnet das Menu Fenster
            Window win3 = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "SignUpWindow");
            Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "LoginWindow");
            Window win2 = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "MainMenu");
            win2.Show();
            if (win3 != null)
            {
                win3.Close();
            }
            else if (win != null)
            {
                win.Close();
            }
        }
    }
}
