﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using JasonsGameApp.CodeDateien;
using JasonsGameApp.Pages;
using JasonsGameApp.Pages.AccountPages;
using JasonsGameApp.Pages.HomeMenu;
using JasonsGameApp.UserControls;
using log4net;
using ToastNotifications.Messages;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für NewMenu.xaml
    /// </summary>
    
    public partial class NewMenu : Window
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private static readonly TaskCodes taskCodes = TaskCodes.Instance;
        string CurrentPath;
        WelcomePage page;

        public NewMenu()
        {
            if (runTimeSave.RetrieveMaintenaceState())
            {
                MaintenanceMode mode = new MaintenanceMode();
                mode.Show();
                this.Close();
                return;
            }
            InitializeComponent();
            log.Info("Menu Initialized");
            runTimeSave.menu = this;
            if (Properties.Settings.Default.SplashScreen == true)
            {
                splashform form = new splashform(this);
                form.ShowDialog();
            }
            if (Properties.Settings.Default.AlwaysLoggedIn == true)
            {
                runTimeSave.UserName = Properties.Settings.Default.UserName;
                runTimeSave.IsLoggedIn = true;
                txtBoxLoggedInUser.Text = "Logged in as: " + runTimeSave.UserName;
            }
            log.Debug("Menu Loaded into MainCodes");
            CustomizeDesign();
            log.Debug("Menu Design Customized");
            CurrentPath = Directory.GetCurrentDirectory();
            page = new WelcomePage();
            mainFrame.Navigate(page);
            log.Info("Showing Welcome Page");
            if (Properties.Settings.Default.ShowUpdateScreen == true)
            {
                Properties.Settings.Default.ShowUpdateScreen = false;
                Properties.Settings.Default.Save();
                UpdateWindow uw = new UpdateWindow();
                uw.Show(); 
            }
            Task.Run(async () => await taskCodes.CatchStartTime());
        }

        public void NewMenuLoadFromlogin()
        {
            mainFrame.Navigate(page);
            UpdateLayout();
        }

        public NewMenu(string Without)
        {
            InitializeComponent();
            log.Info("Instanciated Menu");
            CustomizeDesign();
            CurrentPath = Directory.GetCurrentDirectory();
            mainFrame.Navigate(page);
        }

        private void CustomizeDesign()
        {
            log.Debug("Customizing Design!");
            panelSubWelcome.Visibility = Visibility.Collapsed;
            panelSubMenuGames.Visibility = Visibility.Collapsed;
            panelSubMenuTools.Visibility = Visibility.Collapsed;
        }

        private void hideSubMenu()
        {
            log.Debug("Hiding Sub Menus");
            if (panelSubWelcome.Visibility == Visibility.Visible)
            {
                panelSubWelcome.Visibility = Visibility.Collapsed;
            }
            if (panelSubMenuGames.Visibility == Visibility.Visible)
            {
                panelSubMenuGames.Visibility = Visibility.Collapsed;
            }
            if (panelSubMenuTools.Visibility == Visibility.Visible)
            {
                panelSubMenuTools.Visibility = Visibility.Collapsed;
            }
            if (panelSubMenuWallet.Visibility == Visibility.Visible)
            {
                panelSubMenuWallet.Visibility = Visibility.Collapsed;
            }
            if (panelAccount.Visibility == Visibility.Visible)
            {
                panelAccount.Visibility = Visibility.Collapsed;
            }
            if (runTimeSave.IsLoggedIn == true)
            {
                btnSubAccountManagement.Visibility = Visibility.Visible;
                btnSubLoginSignUp.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowSubMenu(Panel panel)
        {
            log.Debug("Showing SubMenu from panel " + panel.Name);
            if (panel.Visibility == Visibility.Collapsed)
            {
                hideSubMenu();
                panel.Visibility = Visibility.Visible;
            }
            else
            {
                panel.Visibility = Visibility.Collapsed;
            }
        }

        private void btnWelcome_Click(object sender, RoutedEventArgs e)
        {
            ShowSubMenu(panelSubWelcome);
        }

        private void btnSubWelcome_Click(object sender, RoutedEventArgs e)
        {
            
            mainFrame.Navigate(page);
            log.Debug("Showing Welcome Page");
            hideSubMenu();
        }

        private void btnSubInformation_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new About());
            log.Debug("Showing About page");
            hideSubMenu();
        }

        private void btnSubUpdate_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new Update(this));
            log.Debug("Showing Update Page");
            hideSubMenu();
        }

        private void btnSubBlackJack_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new BlackJack());
            log.Debug("Showing BlackJack Page");
            hideSubMenu();
        }

        private void btnSubTicTacToe_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new TicTacToe());
            log.Debug("Showing TicTacToe Page");
            hideSubMenu();
        }

        private void btnSubDokiDokiGer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(CurrentPath + "\\DokiDokiGerman\\DDLC.exe");
                log.Info("Starting DDLC.exe from Path: {AppPath}\\DokiDokiGerman\\DDLC.exe");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Cant Start DokiDoki. Please Check if you have the Permission to run or try to download it from the Download Center, otherwise" +
                    " contact Support with the latest Log File");
                log.Error("Cant start DDLC.exe: ", ex);
            }
            MessageBox.Show("DokiDoki wird gestartet. Bitte haben sie einen moment Geduld.");
            hideSubMenu();
        }

        private void btnGames_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing Games Sub Menu");
            ShowSubMenu(panelSubMenuGames);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing Tools Sub Menu");
            ShowSubMenu(panelSubMenuTools);
        }

        private void btnSubLoginSignUp_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsOnline == false) return;
            Login login = new Login();
            log.Info("Instanciated login Window");
            login.Show();
            this.Hide();
            log.Debug("Closing Menu");
            hideSubMenu();
        }

        private void btnDownloadCenter_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new DownloadCenter());
            log.Debug("Showing Download Center Page");
            hideSubMenu();
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new settings(this));
            log.Debug("Showing Settings Page");
            hideSubMenu();
        }

        private void btnSubPong_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(CurrentPath + "\\Games\\Pong\\Pong.exe");

            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unexpected Exception. Please Report this to an Admin and send the Newest Log File!");
                log.Error("Exception Occured from Opening Game Pong: ", ex);
            }
            log.Info("Starting Pong from path: {APPPATH}\\Games\\Pong\\Pong.exe");
            hideSubMenu();
        }

        private void btnSubEndlessRunner_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(CurrentPath + "\\Games\\EndlessRunner\\EndlessRunner.exe");

            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Unexpected Exception. Please Report this to an Admin and send the Newest Log File!");
                log.Error("Exception Occured from Opening Game Endless Runner: ", ex);
            }
            log.Info("Starting EndlessRunner from path: {APPPATH}\\Games\\EndlessRunner\\EndlessRunner.exe");
            hideSubMenu();
        }

        private void btnFileDeleter_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new FileDeleter());
            log.Debug("Showing FileDeleter Page");
            hideSubMenu();
        }

        private void btnAutostarter_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new AutoStarter());
            log.Debug("Showing Autostarter Page");
            hideSubMenu();
        }

        private void MainMenu_Loaded(object sender, RoutedEventArgs e)
        {
            RunTimeSingletonSave save = RunTimeSingletonSave.Instance;
        }

        private void MainMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.UpdateLayout();
        }

        private void btnWallet_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing Wallet SubMenu");
            if (Properties.Settings.Default.WalletInUse == false)
            {
                if (runTimeSave.IsLoggedIn == false)
                {
                    MessageBox.Show("Bitte melde dich zuerst an!");
                    return;
                }
                mainFrame.Navigate(new WalletSetupPage());
            }
            else
            {
                if (runTimeSave.IsLoggedIn == false)
                {
                    MessageBox.Show("Bitte melde dich zuerst an!");
                    return;
                }
                ShowSubMenu(panelSubMenuWallet);
            }
        }

        private void btnOpenWallet_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing UserWallet Page");
            mainFrame.Navigate(new UserWallet());
            hideSubMenu();
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing Tasks Page");
            mainFrame.Navigate(new Tasks());
            hideSubMenu();
        }

        private void btnSubAccountManagement_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing Account Management Page");
            mainFrame.Navigate(new AccountManagementPage());
            hideSubMenu();
        }

        private void btnAccount_Click(object sender, RoutedEventArgs e)
        {
            log.Debug("Showing Account Sub Menu");
            ShowSubMenu(panelAccount);
        }

        private void btnSearchUser_Click(object sender, RoutedEventArgs e)
        {
            if (runTimeSave.IsLoggedIn == false)
            {
                runTimeSave.notifier.ShowInformation("Bitte melde dich zuerst an!");
                return;
            }
            mainFrame.Navigate(new ListUserPage());
            hideSubMenu();
        }

        private void btnSubSupport_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Navigate(new Support());
            hideSubMenu();
        }
    }
}
