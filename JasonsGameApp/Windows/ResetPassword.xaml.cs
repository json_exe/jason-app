﻿using JasonsGameApp.CodeDateien;
using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace JasonsGameApp.Windows
{
    /// <summary>
    /// Interaktionslogik für ResetPassword.xaml
    /// </summary>
    public partial class ResetPassword : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private bool failed = false;

        public ResetPassword()
        {
            InitializeComponent();
            log.Info("Instanciated ResetPassword Window");
        }

        MySqlConnection connection;
        int VerificationCode;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                failed = true;   
            }
        }

        private void CloseSqlConnection()
        {
            connection.Close();
            log.Debug("Closing SQL-Connection");
        }

        int btnPressed = 0;
        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            //Guckt wie oft der Button gedrückt wurde
            if (btnPressed == 0)
            {
                //Erstellen eines regex um die E-Mail Adresse zu überprüfen
                Regex regexMail = new Regex(@"^[a-zA-Z0-9-_.+#,]{3,25}@\b(gmail|web|gmx|outlook|mail|yahoo)\b[.]\b(com|de)\b$");
                bool IsEmail = regexMail.IsMatch(EnterEMail.Text);
                //Guckt ob die E-Mail richtig ist
                if (IsEmail == true)
                {
                    log.Debug("Email is Valid");
                    //Öffnet eine Datenbank Verbindung
                    OpenSqlConnection();
                    if (failed == true) return;
                    //Sucht in der Datenbank nach der E-Mail
                    string queryMail = "SELECT * FROM AccountInfo WHERE EMail = @EMail";
                    MySqlCommand cmd = new MySqlCommand(queryMail, connection);
                    cmd.Parameters.AddWithValue("@EMail", EnterEMail.Text);
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    log.Debug("Running dataReader");

                    if (dataReader.Read())
                    {
                        //Schickt eine Verifizierungsemail wenn die E-Mail existiert
                        log.Debug("E-Mail Found");
                        CloseSqlConnection();
                        Random rndm = new Random();
                        VerificationCode = rndm.Next(100000, 999999);
                        VerfyMail(VerificationCode, EnterEMail.Text);
                        log.Debug("Sended Verification Mail!");
                        EnterEMail.Visibility = Visibility.Hidden;
                        EnterVerifyCode.Visibility = Visibility.Visible;
                        txtBlockInstructions.Text = "Wir haben dir nun einen Verifizierungscode gesendet, um zu Bestätigen das dies auch wirklich " +
                            "du bist! Bitte gebe den Code aus der E-Mail in dem Eingabefeld ein.";
                        btnPressed++;
                    }
                    else
                    {
                        log.Debug("No E-Mail Adress Found");
                        MessageBox.Show("E-Mail Adresse ist nicht in unserem System vorhanden! Bitte überprüfe nochmal deine E-Mail und versuche " +
                            "es erneut!");
                        CloseSqlConnection();
                    }
                }
                else
                {
                    log.Debug("E-Mail Adress Invalid");
                    MessageBox.Show("Bitte gebe eine Valide E-Mail Adresse ein!", "E-Mail wrong", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else if (btnPressed == 1)
            {
                //Erstellen eines Regex zum Überprüfen des Verifizierungscodes
                Regex VerifyCode = new Regex("^[0-9]{6}$");
                bool IsVerificationCode = VerifyCode.IsMatch(EnterVerifyCode.Text);
                if (IsVerificationCode == true)
                {
                    log.Debug("Valid Verification Code");
                    if (VerificationCode == int.Parse(EnterVerifyCode.Text))
                    {
                        //Wenn der Verifizierungscode krorekt ist, lädt die nächste Seite
                        log.Debug("Verification Code Correct");
                        EnterVerifyCode.Visibility = Visibility.Hidden;
                        lblEnterNewPassword.Visibility = Visibility.Visible;
                        lblReEnterNewPassword.Visibility = Visibility.Visible;
                        pswdBoxEnterNewPassword.Visibility = Visibility.Visible;
                        pswdBoxReEnterNewPassword.Visibility = Visibility.Visible;
                        txtBlockInstructions.Text = "Bitte gebe nun dein Passwort ein und drücke auf Weiter um es zu bestätigen!";
                        btnPressed++;
                    }
                    else
                    {
                        log.Debug("Incorrect Verification Code");
                        MessageBox.Show("Der Verifizierungscode stimmt nicht überein! Bitte überprüfen deine Eingaben erneut!");
                    }
                }
                else
                {
                    log.Debug("Invalid Verification Code");
                    MessageBox.Show("Bitte gebe einen Validen Verifizierungscode ein! Dieser besteht aus 6 Zahlen.");
                }
            }
            else if (btnPressed == 2)
            {
                //Erstellen eines Regex um das Passwort auf die Kriterien zu überprüfen
                Regex regexPswd = new Regex("^[a-zA-Z0-9_-]{8,50}$");
                bool IsPassword = regexPswd.IsMatch(pswdBoxEnterNewPassword.Password);
                if (IsPassword == true)
                {
                    log.Debug("Valid Password");
                    if (pswdBoxEnterNewPassword.Password == pswdBoxReEnterNewPassword.Password)
                    {
                        //Setzt beim Nutzer das neue Passwort
                        OpenSqlConnection();
                        string queryUpdatePswd = "UPDATE AccountInfo SET Password = @Password WHERE EMail = @EMail";
                        MySqlCommand cmd2 = new MySqlCommand(queryUpdatePswd, connection);
                        cmd2.Parameters.AddWithValue("@EMail", EnterEMail.Text);
                        cmd2.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(pswdBoxEnterNewPassword.Password));
                        try
                        {
                            cmd2.ExecuteNonQuery();
                            log.Debug("Successfully Updated Password");
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to Execute SQL-Command with query UpdatePswd!", ex);
                            MessageBox.Show("Failed to Change Password! Please check: \n -That you have a Inet Connection \n -That on the App Discord" +
                                            " that the Server isnt Down at the moment \n -Retry a second Time \n Otherwise please contact Support " +
                                            "with the newest Log File!", "Exception Occured", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        MessageBox.Show("Passwort wurde geändert! Sie können sich nun mit den neuen Daten beim Login Anmelden!");
                        CloseSqlConnection();
                        //Sendet eine E-Mail
                        EmailPasswordChanged(EnterEMail.Text);
                        log.Debug("Notification Mail sended");
                        Login login = new Login();
                        login.Show();
                        this.Close();
                    }
                    else
                    {
                        log.Debug("Pswds are not the Same");
                        MessageBox.Show("Passwörter stimmen nicht überein! Bitte überprüfe deine Eingaben.");
                    }
                }
                else
                {
                    log.Debug("Pswd doesnt match our criteria");
                    MessageBox.Show("Dein Passwort entspricht nicht unseren Kriterien! Bitte versuche es erneut.");
                }
                
            }
            else
            {
                log.Error("You should not get here...");
            }
        }

        //Methode in der die Verifizierungsmail definiert wird
        private void VerfyMail(int VerifyCode, string EMail)
        {
            try
            {
                log.Debug("Start Sending VerifyMail!");
                WebClient webClient = new WebClient();
                //Erstellen einer neuen E-Mail Nachricht
                MailMessage message = new MailMessage();
                //Erstellen eines neuen Smtp Clients zum Versenden der Mail
                SmtpClient smtpClient = new SmtpClient();
                message.From = new MailAddress("jasondevelopmentstudio@gmail.com");
                //Deklarieren wer der Empfänger ist
                message.To.Add(new MailAddress(EMail));
                //Thema der Mail deklarieren.
                message.Subject = "Passwort zurücksetzen. Code: " + VerifyCode;
                //Load HTML file from Server Cloud
                var htmlData = webClient.DownloadData("https://jsprivatenextcloud.ddns.net/index.php/s/StzZLCtoGnLQ39B/download/pswdresetmail.html");
                var htmlCode = Encoding.UTF8.GetString(htmlData);
                //Deklarieren wie die Mail aussieht in HTML
                message.Body = htmlCode;
                //Text Encoder UTF8 mitgeben damit auch Buchstaben wie ß dargestellt werden
                message.SubjectEncoding = Encoding.UTF8;
                //Sagen das der E-Mail Inhalt in HTMl ist
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;
                //Port und Server für den smtp Client festlegen
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                //Anmeldeinformationen für den Client festlegen
                smtpClient.Credentials = new NetworkCredential("jasondevelopmentstudio@gmail.com", "purs xzzz ubtk evqj");
                //Mail senden
                smtpClient.Send(message);
                log.Debug("Verify Mail sended");
            }
            catch (Exception ex)
            {
                log.Error("Failed to Send Verification Mail! ERRORCODE: RP188", ex);
                MessageBox.Show("Fehler beim senden der VerifizierungsMail! Bitte üperprüfen sie: \n -Das sie eine Inet Verbindung haben \n " +
                    "-Das es keine Ankündigung auf dem Offiziellen Discord gibt, das der Server Offline ist \n Sollte Ihnen all dies nicht" +
                    " weiterhelfen, kontaktieren sie bitte den Support und senden sie die neuste Log Datei mit! ", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Methode die, die PasswordChanged Email sendet
        private void EmailPasswordChanged(string EMail)
        {
            try
            {
                log.Debug("Start sending Notification mail");
                WebClient webClient = new WebClient();
                //Erstellen einer neuen E-Mail Nachricht
                MailMessage message = new MailMessage();
                //Erstellen eines neuen Smtp Clients zum Versenden der Mail
                SmtpClient smtpClient = new SmtpClient();
                message.From = new MailAddress("jasondevelopmentstudio@gmail.com");
                //Deklarieren wer der Empfänger ist
                message.To.Add(new MailAddress(EMail));
                //Thema der Mail deklarieren.
                message.Subject = "Passwort geändert";
                //Read HTML Code from File on Server-Cloud
                var htmlData = webClient.DownloadData("https://jsprivatenextcloud.ddns.net/index.php/s/QrzbEqJCGNrckG3/download/pswdresettet.html");
                var htmlCode = Encoding.UTF8.GetString(htmlData);
                //Deklarieren wie die Mail aussieht in HTML
                message.Body = htmlCode;
                //Text Encoder UTF8 mitgeben damit auch Buchstaben wie ß dargestellt werden
                message.SubjectEncoding = Encoding.UTF8;
                //Sagen das der E-Mail Inhalt in HTMl ist
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;
                //Port und Server für den smtp Client festlegen
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                //Anmeldeinformationen für den Client festlegen
                smtpClient.Credentials = new NetworkCredential("jasondevelopmentstudio@gmail.com", "purs xzzz ubtk evqj");
                //Mail senden
                smtpClient.Send(message);
                log.Debug("Sended Notify Mail");
            }
            catch (Exception ex)
            {
                //Falls fehler auftreten werden diese in einer TextBox ausgegeben
                log.Error("Failed to send Notification Mail! ERRORCODE: RP232", ex);
                MessageBox.Show("Es gab ein Fehler beim senden der Informations Mail. Falls dies öfters passirt kontaktieren sie bitte " +
                    "den Support und senden sie die neuste Log Datei mit!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Methode um das Passwort zu verschlüsseln
        private string PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Start Encrypting Pswd");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Pswd Encrypted");
                return builder.ToString();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            runTimeSave.menu.Show();
        }
    }
}
