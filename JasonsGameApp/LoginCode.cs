﻿using JasonsGameApp.CodeDateien;
using JasonsGameApp.Windows;
using MySql.Data.MySqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using ToastNotifications.Messages;

namespace JasonsGameApp
{
    
    public class LoginCode
    {
        public bool LoggedIn;
        MySqlConnection connection;
        public string UserAccountName;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static RunTimeSingletonSave runTimeSave = RunTimeSingletonSave.Instance;
        private static TaskCodes taskCodes = TaskCodes.Instance;
        bool failed = false;

        private void OpenSqlConnection()
        {
            Stream stream = runTimeSave.ServerClient.OpenRead(Properties.Settings.Default.ServerURL);
            StreamReader reader = new StreamReader(stream);
            connection = new MySqlConnection(reader.ReadToEnd());
            try
            {
                connection.Open();
                log.Debug("Connection Established");
            }
            catch (System.Exception ex)
            {
                log.Error("Cant Open Connection to MySQL Database: ", ex);
                MessageBox.Show("Es konnte keine Verbindung zur Datenbank hergestellt werden. Dies kann mehrere Ursachen haben." +
                    " Bitte überprüfen sie: -Das eine Inet. Verbindung vorhanden ist \n -Es keine Info auf dem Offiziellen Discord" +
                    " gab, das der Server momentan durch Wartung Offline ist \n -Sollte das Problem weiterhin bestehen wenden" +
                    "sie sich bitte an den Support und senden sie neuste Log Datei mit!");
                failed = true;
            }
        }

        private void CloseSqlConnection()
        {
            connection.Close();
            log.Debug("Closing SQL-Connection");
        }

        public void PerformLogin(Login login)
        {
            OpenSqlConnection();
            if (failed == true) return;
            string queryCheckUserName = "SELECT * FROM AccountInfo WHERE UserName = @UserName";
            MySqlCommand cmd = new MySqlCommand(queryCheckUserName, connection);
            cmd.Parameters.AddWithValue("@UserName", login.txtUsername.Text);
            MySqlDataReader readUserName = cmd.ExecuteReader();
            log.Debug("Creating readUserName SQL-Reader");
            if (readUserName.Read())
            {

                readUserName.Close();
                log.Debug("Closing readUserName Reader");
                string queryCheckPassword = "SELECT * FROM AccountInfo WHERE UserName = @UserName and Password = @Password";
                MySqlCommand cmd2 = new MySqlCommand(queryCheckPassword, connection);
                cmd2.Parameters.AddWithValue("@UserName", login.txtUsername.Text);
                cmd2.Parameters.AddWithValue("@Password", PasswortVerschlüsseln(login.EnterPSWD.Password));
                MySqlDataReader success = cmd2.ExecuteReader();
                log.Debug("Creating succes SQL Reader");

                if (success.Read())
                {
                    runTimeSave.IsLoggedIn = true;
                    runTimeSave.UserName = login.txtUsername.Text;
                    NewsCode newsCode = new NewsCode();
                    newsCode.RetrieveWalletState();
                    runTimeSave.RetrieveUserID();
                    taskCodes.UserLogsIn = true;
                    runTimeSave.notifier.ShowInformation($"Willkommen zurück {runTimeSave.UserName}!");
                    if (login.checkBoxAlwaysLoggedIn.IsChecked == true)
                    {
                        Properties.Settings.Default.AlwaysLoggedIn = true;
                        Properties.Settings.Default.UserName = login.txtUsername.Text;
                        Properties.Settings.Default.Save();
                    }
                    //MainWindow aufrufen
                    Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Name == "MainMenu");
                    win.Show();
                    runTimeSave.menu.txtBoxLoggedInUser.Text = "Logged in as: " + login.txtUsername.Text;
                    runTimeSave.menu.NewMenuLoadFromlogin();
                    log.Debug("Showing Menu");
                    login.Close();
                    log.Debug("Closing Login");
                }
                else
                {
                    MessageBox.Show("Passwort ist Falsch! Bitte versichern sie sich, das sie Ihr Passwort richtig " +
                        "eingetragen haben!");
                    log.Info("User Password is Wrong");
                }
            }
            else
            {
                MessageBox.Show("Benutzername existiert nicht! Bitte vergewissern sie sich, das der eingegebene " +
                    "Benutzername korrekt ist. Ansonsten erstellen sie erst ein Account.");
                log.Info("User UserName is Wrong");
            }
            CloseSqlConnection();
        }

        private string PasswortVerschlüsseln(string pwd)
        {
            //Erstellen von SHA256
            using (SHA256 sha256 = SHA256.Create())
            {
                log.Debug("Encrypting Password");
                //Passwort hashen
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                //Byte Array zu einem String konvertieren
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                log.Info("Password Encrypted");
                return builder.ToString();
            }
        }
    }
}
